import type { Observable } from 'rxjs';

export interface Message {
  topic: string;
  payload: string | Buffer;
}

export interface IotConnection {
  readonly clientId: string;
  subscribe(topic: string): Promise<Observable<Message>>;
  publish(topic: string, payload: string): Promise<void>;
}

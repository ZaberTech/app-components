/* eslint-disable @typescript-eslint/no-this-alias */

import { Subject } from 'rxjs';

let wrapperInstance: MqttWrapperMock;

class MqttWrapperMock {
  close = jest.fn();
  connect = jest.fn().mockResolvedValue(undefined);
  closed = new Subject();

  constructor(readonly defaultRealm: string, readonly clientId: string) {
    wrapperInstance = this;
  }
}

jest.mock('./mqtt_wrapper', () => ({
  get MqttWrapper() {
    return MqttWrapperMock;
  },
}));
jest.mock('uuid', () => ({
  get v4() {
    return () => 'uuid';
  },
}));

import { Container, injectable } from 'inversify';

import { createContainer, destroyContainer } from '../test';
import { AwsCredentialsService } from '../aws';
import { Config } from '../config';

import { IoTService } from './iot';

beforeAll(() => {
  Config.merge({
    aws: {
      iotClientIdPrefix: 'prefix',
    }
  });
});

function mockCredentials(identityId = 'identity', authenticated = true) {
  return {
    authenticated,
    cognito: {
      identityId,
      accessKeyId: 'key',
      secretAccessKey: 'secret',
      sessionToken: 'session',
    }
  };
}

@injectable()
class CredentialsMock {
  renewCredentials = new Subject<void>();
  getCredentials = jest.fn().mockResolvedValue(mockCredentials());
}

let container: Container;
let iot: IoTService;
let credentials: CredentialsMock;

beforeEach(() => {
  container = createContainer();
  container.bind<unknown>(AwsCredentialsService).to(CredentialsMock);
  credentials = container.get<unknown>(AwsCredentialsService) as CredentialsMock;
  iot = container.get(IoTService);
});

afterEach(() => {
  destroyContainer();
  container = null!;

  iot = null!;
  wrapperInstance = null!;
});

describe('getConnection', () => {
  test('initializes the mqtt connection from provided credentials (authenticated)', async () => {
    await iot.getConnection();
    expect(wrapperInstance.defaultRealm).toBe('identity');
    expect(wrapperInstance.clientId).toBe('prefix-uuid');
    expect(wrapperInstance.connect).toBeCalledWith(expect.objectContaining({
      accessKeyId: 'key',
      secretAccessKey: 'secret',
      sessionToken: 'session',
    }));
  });

  test('constructs the mqtt connection from credentials only once', async () => {
    const [connection1, connection2] = await Promise.all([iot.getConnection(), iot.getConnection()]);

    expect(connection1 === connection2).toBe(true);
    expect(credentials.getCredentials).toHaveBeenCalledTimes(1);
  });

  test('allows repeated attempts to succeed (credentials failed)', async () => {
    credentials.getCredentials.mockRejectedValueOnce(new Error('Cannot get credentials'));

    await expect(iot.getConnection()).rejects.toThrow('Cannot get credentials');
    await iot.getConnection();
  });

  test('reconnects when connection is lost', async () => {
    const oldConnection = await iot.getConnection();

    wrapperInstance.closed.next(undefined);
    wrapperInstance.closed.complete();

    const newConnection = await iot.getConnection();

    expect(oldConnection !== newConnection).toBe(true);
    expect(credentials.getCredentials).toHaveBeenCalledTimes(2);
  });
});

describe('renew credentials', () => {
  test('creates new connection and closes the old one when new credentials are issued', async () => {
    const oldConnection = await iot.getConnection();
    const oldInstance = wrapperInstance;

    credentials.getCredentials.mockResolvedValue(mockCredentials('identity2', false));
    credentials.renewCredentials.next();

    const newConnection = await iot.getConnection();

    expect(oldConnection !== newConnection).toBe(true);
    expect(wrapperInstance.defaultRealm).toBe('unauthenticated');
    expect(wrapperInstance.clientId).toBe('prefix-uuid');
    expect(oldInstance.close).toHaveBeenCalled();
  });
});

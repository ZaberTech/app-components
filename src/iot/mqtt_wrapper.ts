import { Observable, ReplaySubject, Subject } from 'rxjs';
import { filter, share, merge } from 'rxjs/operators';
import mqtt from 'mqtt';
import { createIoTWebsocketUrl } from '@zaber/aws-crt-utils';

import { Logger } from '../log';
import { Config } from '../config';

import { Message } from './types';

const QOS = 0;

interface Credentials {
  accessKeyId: string;
  secretAccessKey: string;
  sessionToken: string;
}

export class MqttWrapper {
  private _mqtt!: mqtt.MqttClient;
  private readonly messages = new Subject<Message>();

  private readonly _closed = new ReplaySubject<void>();
  public readonly closed: Observable<void> = this._closed;

  constructor(
    public readonly defaultRealm: string,
    public readonly clientId: string,
    private readonly logger: Logger,
  ) { }

  public connect({ accessKeyId, secretAccessKey, sessionToken }: Credentials): Promise<void> {
    const hostName = Config.current.aws.iotCoreHost;
    if (!hostName) { throw new Error('aws.iotCoreHost not specified') }
    const region = Config.current.aws.region;
    if (!region) { throw new Error('aws.region not specified') }

    const url = createIoTWebsocketUrl({
      hostName,
      port: 443,
      region,
      accessKeyId,
      secretAccessKey,
      sessionToken,
    });

    this._mqtt = mqtt.connect(url, {
      clientId: `${this.defaultRealm}-${this.clientId}`,
      protocol: 'wss',
      reconnectPeriod: -1,
      resubscribe: false,
    });

    this._mqtt.on('message', (topic, payload) =>
      this.messages.next({ topic, payload })
    );
    this._mqtt.on('error', error => {
      this.onError(new Error(`Connection to IoT failed: ${error}`));
      this.logger.warn('Connection error', error);
    });
    this._mqtt.on('close', () => {
      this.onError(new Error('The connection has been closed'));
      this.logger.info('Closed');
    });
    this._mqtt.on('connect', () => {
      this.logger.info('Connected');
    });

    return new Promise((resolve, reject) => {
      this._mqtt.once('connect', resolve);
      this._mqtt.once('error', reject);
      this._mqtt.once('close', () => reject(new Error('The connection has been closed')));
    });
  }

  private onError(err: Error): void {
    if (!this.messages.hasError) {
      this.messages.error(err);
    }
    if (!this._closed.isStopped) {
      this._closed.next();
      this._closed.complete();
    }
  }

  public close(): void {
    this._mqtt.end(true);
  }

  /**
   * Subscribes to MQTT topic returning hot shareable observable with matching messages.
   * The observable needs to be subscribed after the method returns.
   * Don't forget to unsubscribe from the observable to also end the MQTT subscription.
   *
   * Why Promise<Observable<>>? Because the calling code needs to know once the subscription is active before publishing.
   */
  public async subscribe(topic: string): Promise<Observable<Message>> {
    const matchingRegexp = new RegExp(`^${topic}$`.replace('/', '\\/').replace('+', '\\w+').replace('#$', ''));

    const unsubscribe = () => this._mqtt.unsubscribe(topic);
    await new Promise<void>((resolve, reject) =>
      this._mqtt.subscribe(topic, { qos: QOS }, err => err ? reject(err) : resolve())
    );

    let wasSubscribed = false;
    let isCancelled = false;
    setImmediate(() => {
      if (wasSubscribed) {
        return;
      }
      isCancelled = true;
      unsubscribe();
      this.logger.warn(`Observable for topic ${topic} has not been subscribed. Unsubscribing to prevent leak.`);
    });

    return new Observable<Message>(() => {
      if (wasSubscribed) {
        throw new Error('Repeated subscriptions are not allowed. Call subscribe method of IoT again.');
      }
      if (isCancelled) {
        throw new Error('The observable has not been subscribed immediately and therefore it\'s been cancelled.');
      }
      wasSubscribed = true;
      return unsubscribe;
    }).pipe(
      merge(this.messages),
      filter(message => matchingRegexp.test(message.topic)),
      share(),
    );
  }

  public publish(topic: string, payload: string | Buffer): Promise<void> {
    return new Promise((resolve, reject) => {
      this._mqtt.publish(topic, payload, { qos: QOS }, err => err ? reject(err) : resolve());
    });
  }
}

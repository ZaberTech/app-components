import { v4 as uuidv4 } from 'uuid';
import { inject, injectable } from 'inversify';

import { Log, Logger } from '../log';
import { AwsCredentialsService } from '../aws';
import { Config } from '../config';

import { MqttWrapper } from './mqtt_wrapper';
import { IotConnection } from './types';

const UNAUTHENTICATED = 'unauthenticated';

@injectable()
export class IoTService {
  private logger: Logger;
  private connectionPromise: Promise<MqttWrapper> | null = null;
  private clientId: string | null = null;
  private currentConnection: MqttWrapper | null = null;

  constructor(
    @inject(Log) log: Log,
    @inject(AwsCredentialsService) private readonly credentials: AwsCredentialsService,
  ) {
    this.logger = log.getLogger('IoTService');
    credentials.renewCredentials.subscribe(this.onNewCredentials.bind(this));
  }

  public getConnection(): Promise<IotConnection> {
    if (!this.connectionPromise) {
      this.connectionPromise = this.openConnection();
      this.connectionPromise.catch(() => this.resetConnection());
    }

    return this.connectionPromise;
  }

  private async openConnection(): Promise<MqttWrapper> {
    const credentials = await this.credentials.getCredentials();

    const defaultRealm = credentials.authenticated ? credentials.cognito.identityId : UNAUTHENTICATED;
    if (!this.clientId) {
      this.clientId = `${Config.current.aws.iotClientIdPrefix ?? 'unknown'}-${uuidv4()}`;
    }

    const connection = new MqttWrapper(defaultRealm, this.clientId, this.logger);
    connection.closed.subscribe(() => {
      if (this.currentConnection === connection) {
        this.resetConnection();
      }
    });
    this.currentConnection = connection;

    await connection.connect(credentials.cognito);

    return connection;
  }

  private onNewCredentials() {
    this.clientId = null;

    const connection = this.currentConnection;
    this.resetConnection();

    connection?.close();
  }

  private resetConnection() {
    this.currentConnection = null;
    this.connectionPromise = null;
  }
}

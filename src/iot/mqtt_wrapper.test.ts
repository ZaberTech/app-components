/* eslint-disable @typescript-eslint/no-this-alias */

import { EventEmitter } from 'events';

let connectionMock: Connection;

class Connection extends EventEmitter {
  constructor() {
    super();
    connectionMock = this;
  }

  publish = jest.fn(
    (topic: string, payload: string | Buffer, options: unknown, callback: (err: Error | null) => void) => { setImmediate(callback, null) }
  );

  subscribe = jest.fn(
    (topic: string, options: unknown, callback: (err: Error | null) => void) => { setImmediate(callback, null) }
  );
  unsubscribe = jest.fn();

  end = jest.fn();
}

const connectMock = jest.fn(() => new Connection());
jest.mock('mqtt', () => ({
  get connect(): unknown {
    return connectMock;
  },
}));

const prepareWebSocketUrl = jest.fn<string, [any]>(() => 'iot-url');
jest.mock('@zaber/aws-crt-utils', () => ({
  get createIoTWebsocketUrl(): unknown {
    return prepareWebSocketUrl;
  },
}));

import { Logger } from '../log';
import { waitTick } from '../test';
import { Config } from '../config';

import { MqttWrapper } from './mqtt_wrapper';
import { Message } from './types';

beforeAll(() => {
  Config.merge({
    aws: {
      iotCoreHost: 'iot-host',
      region: 'region',
    }
  });
});

const loggerMock = {
  warn: jest.fn(),
  info: jest.fn(),
};

let wrapper: MqttWrapper;
beforeEach(() => {
  wrapper = new MqttWrapper('realm', 'client', loggerMock as unknown as Logger);
});

afterEach(() => {
  connectionMock = null!;
  wrapper = null!;

  prepareWebSocketUrl.mockClear();
  connectMock.mockClear();
});

const CREDENTIALS = { accessKeyId: 'access', secretAccessKey: 'secret', sessionToken: 'session' };

describe('connect', () => {
  test('constructs connection passing down credentials', async () => {
    const connectPromise = wrapper.connect(CREDENTIALS);

    expect(connectMock).toHaveBeenCalledWith('iot-url', expect.objectContaining({
      clientId: 'realm-client',
      reconnectPeriod: -1,
    }));
    expect(prepareWebSocketUrl).toHaveBeenCalledWith({
      hostName: 'iot-host',
      port: 443,
      region: 'region',
      accessKeyId: 'access',
      secretAccessKey: 'secret',
      sessionToken: 'session',
    });
    connectionMock.emit('connect');

    await connectPromise;
  });
  test('throws error if connection fails to connect', async () => {
    const connectPromise = wrapper.connect(CREDENTIALS);
    connectionMock.emit('error', new Error('Cannot connect'));
    await expect(connectPromise).rejects.toThrow('Cannot connect');
  });
  test('throws error if connection is closed before the connection promise is completed', async () => {
    const connectPromise = wrapper.connect(CREDENTIALS);
    connectionMock.emit('close');
    await expect(connectPromise).rejects.toThrow(/has been closed/);
  });
});

describe('once connected', () => {
  beforeEach(async () => {
    const connectPromise = wrapper.connect(CREDENTIALS);
    connectionMock.emit('connect');
    await connectPromise;
  });

  describe('publish', () => {
    test('calls publish of the connection', async () => {
      await wrapper.publish('topic', 'payload');
      expect(connectionMock.publish).toHaveBeenCalledWith('topic', 'payload', { qos: 0 }, expect.anything());
    });

    test('publishes binary data', async () => {
      const payload = Buffer.from([1, 2, 3]);
      await wrapper.publish('topic', payload);
      expect(connectionMock.publish).toHaveBeenCalledWith('topic', payload, { qos: 0 }, expect.anything());
    });

    test('handles error of the connection', async () => {
      connectionMock.publish.mockImplementationOnce((topic, message, options, callback) => callback(new Error('Cannot send')));
      const promise = wrapper.publish('topic', 'payload');
      await expect(promise).rejects.toThrow('Cannot send');
    });
  });

  describe('close', () => {
    test('calls end of the connection', async () => {
      wrapper.close();
      expect(connectionMock.end).toHaveBeenCalledWith(true);
    });
  });

  describe('closed', () => {
    test('emits on error', async () => {
      const closedPromise = wrapper.closed.toPromise();
      connectionMock.emit('error', new Error('disconnect'));
      await closedPromise;
    });
    test('emits on close', async () => {
      const closedPromise = wrapper.closed.toPromise();
      connectionMock.emit('close');
      await closedPromise;
    });
  });

  describe('subscribe', () => {
    test('subscribes, receives messages and unsubscribes using the connection', async () => {
      const messages = await wrapper.subscribe('realm/topic');
      expect(connectionMock.subscribe).toHaveBeenCalledWith('realm/topic', { qos: 0 }, expect.anything());

      const received: Message[] = [];
      const subscription = messages.subscribe(message => received.push(message));
      connectionMock.emit('message', 'realm/topic', 'payload');

      expect(received).toEqual([{ topic: 'realm/topic', payload: 'payload' }]);

      subscription.unsubscribe();
      expect(connectionMock.unsubscribe).toHaveBeenCalledWith('realm/topic');
    });

    test('receives binary messages', async () => {
      const payload = Buffer.from([1, 2, 3]);
      const messages = await wrapper.subscribe('realm/topic');
      const received: Message[] = [];
      const subscription = messages.subscribe(message => received.push(message));
      connectionMock.emit('message', 'realm/topic', payload);

      expect(received).toEqual([{ topic: 'realm/topic', payload }]);

      subscription.unsubscribe();
    });

    test('fails the subscribe promise if the underlying subscribe does not succeed', async () => {
      connectionMock.subscribe.mockImplementationOnce((topic, options, callback) => setImmediate(callback, new Error('Cannot subscribe')));
      const promise = wrapper.subscribe('realm/topic');
      await expect(promise).rejects.toThrow('Cannot subscribe');
    });
    test('errors the observable if the connection disconnects later', async () => {
      const messages = await wrapper.subscribe('realm/topic');
      const errorObserver = jest.fn();
      messages.subscribe({
        error: errorObserver,
      });

      connectionMock.emit('error');

      expect(errorObserver).toHaveBeenCalledWith(expect.objectContaining({
        message: expect.stringMatching('Connection to IoT failed'),
      }));
    });
    test('errors the observable if the connection is closed', async () => {
      const messages = await wrapper.subscribe('realm/topic');
      const errorObserver = jest.fn();
      messages.subscribe({
        error: errorObserver,
      });

      connectionMock.emit('close');

      expect(errorObserver).toHaveBeenCalledWith(expect.objectContaining({
        message: expect.stringMatching('The connection has been closed'),
      }));
    });
    test('returned observable can be shared but is usable only once', async () => {
      const messages = await wrapper.subscribe('realm/topic');
      const messagesObserver1 = jest.fn();
      const subscription1 = messages.subscribe({
        next: messagesObserver1,
      });

      connectionMock.emit('message', 'realm/topic', 'payload');

      const messagesObserver2 = jest.fn();
      const subscription2 = messages.subscribe({
        next: messagesObserver2,
      });

      connectionMock.emit('message', 'realm/topic', 'payload');

      subscription1.unsubscribe();
      subscription2.unsubscribe();

      const errorObserver = jest.fn();
      messages.subscribe({
        error: errorObserver,
      });

      expect(messagesObserver1).toHaveBeenCalledTimes(2);
      expect(messagesObserver2).toHaveBeenCalledTimes(1);

      expect(errorObserver).toHaveBeenCalledWith(expect.objectContaining({
        message: expect.stringMatching('Repeated subscriptions are not allowed. Call subscribe method of IoT again.'),
      }));
    });
    test('unsubscribes from MQTT if observable is not subscribed immediately and throws error on future subscribe', async () => {
      const messages = await wrapper.subscribe('realm/topic');

      await waitTick();

      expect(connectionMock.unsubscribe).toHaveBeenCalledTimes(1);

      const errorObserver = jest.fn();
      messages.subscribe({
        error: errorObserver,
      });
      expect(errorObserver).toHaveBeenCalledWith(expect.objectContaining({
        message: expect.stringMatching('The observable has not been subscribed immediately and therefore it\'s been cancelled.'),
      }));
    });
    test('matches emitted message according to MQTT wild cards', async () => {
      async function subscribe(topic: string): Promise<string[]> {
        const topics: string[] = [];
        (await wrapper.subscribe(topic)).subscribe(message => topics.push(message.topic));
        return topics;
      }

      const topicsBasic = await subscribe('realm/topic');
      const topicsWildMiddle = await subscribe('realm/+/stuff');
      const topicsAllSuffix = await subscribe('realm/#');

      const testTopics = ['realm/topic', 'realm/x/stuff', 'realm/something', 'realm/y/stuff', 'something-else'];
      for (const topic of testTopics) {
        connectionMock.emit('message', topic, 'payload');
      }

      expect(topicsBasic).toEqual(['realm/topic']);
      expect(topicsWildMiddle).toEqual(['realm/x/stuff', 'realm/y/stuff']);
      expect(topicsAllSuffix).toEqual(['realm/topic', 'realm/x/stuff', 'realm/something', 'realm/y/stuff']);
    });
  });
});

import type { Nullable } from '@zaber/toolbox';
import type { FirmwareVersion } from '@zaber/motion';

import { Version } from './types';


type VersionCategory = 'public' | 'other' | 'internal';

export function compareVersions(v1: Nullable<Version>, v2: Nullable<Version>): number {
  return compareFirmwareVersions(v1?.parsed, v2?.parsed);
}

export function versionCategory(version: FirmwareVersion): VersionCategory {
  if (version.major === 0 || version.minor >= 99) {
    return 'internal';
  } else if (version.minor >= 95) {
    return 'other';
  } else {
    return 'public';
  }
}

export function compareFirmwareVersions(fwv1: Nullable<FirmwareVersion>, fwv2: Nullable<FirmwareVersion>): number {
  if (!fwv1 && !fwv2) {
    return 0;
  } else if (!fwv1) {
    return -1;
  } else if (!fwv2) {
    return 1;
  }

  if (fwv1.major !== fwv2.major) {
    return fwv1.major - fwv2.major;
  }

  if (fwv1.minor !== fwv2.minor) {
    return fwv1.minor - fwv2.minor;
  }

  return fwv1.build - fwv2.build;
}

export function parseFirmwareVersion(version: string): FirmwareVersion {
  const [major, minor, ...rest] = version.split('.').map(Number);
  return { major, minor, build: rest.at(0) ?? 0 };
}

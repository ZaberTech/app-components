import { FirmwareVersion } from '@zaber/motion';

/**
 * Used to identify the device that is being queried for. These use a different style than most other interfaces in our code
 * because the server they are sent to is case sensitive, requiring they appear exactly as below.
 * */
export interface DeviceIdentifiers {
  /** The device's current device number in the chain. */
  DeviceNumber: number;
  /** The device's device ID, returned by the "get device.id" command. */
  DeviceID: number;
  /** The device's serial number, returned by the "get system.serial" command. */
  SerialNumber: number;
  /** The device's platform ID, returned by the "get system.platform" command. */
  PlatformID: number | null;
  /** Indicates whether or not the device requires a keyed (encrypted) firmware image */
  IsKeyed: boolean;
}

export interface Version {
  parsed: FirmwareVersion;
  str: string;
  notes?: string;
}


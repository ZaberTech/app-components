import * as os from 'os';
import * as path from 'path';
import * as fs from 'fs';

import type { FirmwareVersion } from '@zaber/motion';
import _ from 'lodash';
import { injectable } from 'inversify';

import { fetch } from '../request';
import { Config } from '../config';

import { DeviceIdentifiers, Version } from './types';
import { makeString } from '@zaber/toolbox';


export enum QueryType {
  ServerInfo = 'ServerInfo',
  ListVersions = 'ListVersions',
  Package = 'Package',
  ReportInstall = 'ReportInstall',
}

export interface QueryFirmwareVersion extends DeviceIdentifiers {
  /** The requested firmware version, eg "6.18" */
  FWVersion: string;
  /** The requested build number. */
  BuildNumber: number;
}

export type VersionList = Record<string, Version[]>;

function toFirmwareVersion(version: ApiVersion): FirmwareVersion {
  let major = 0;
  let minor = 0;
  const versionMatch = version.FWVersion.match(/^(\d+)\.(\d+)/);
  if (versionMatch) {
    major = parseInt(versionMatch[1], 10);
    minor = parseInt(versionMatch[2], 10);
  }
  return { major, minor, build: version.Build };
}

interface QueryResponse {
  Success: boolean;
  Message?: string;
}
interface ListVersionsResponse extends QueryResponse {
  ProtocolVersion: number;
  Versions?: Record<string, ApiVersion[]>;
}
interface ApiVersion {
  Build: number;
  FWVersion: string;
  Notes: string;
}

const INTERNAL_URL = 'https://fwee-firmware.izaber.com:7087';
const PUBLIC_URL = 'https://www.zaber.com/support/firmware-self-serve.php';

@injectable()
export class FirmwareWebQuery {
  private readonly baseUrl;

  constructor() {
    this.baseUrl = Config.current.zaber.firmwareUpgradeServer === 'internal' ? INTERNAL_URL : PUBLIC_URL;
  }

  private async fetchFromFirmwareEndpoint(payload: unknown): Promise<Response> {
    const stringOfPayload = JSON.stringify(payload);
    const response = await fetch(this.baseUrl, {
      method: 'POST',
      body: stringOfPayload,
      headers: {
        'Content-Type': 'application/json',
      },
    });
    if (!response.ok) {
      throw Error(`Could not fetch from ${this.baseUrl} with parameters ${stringOfPayload}. Received response: ${response.statusText}.`);
    }
    return response;
  }

  public async getVersions(identifiers: DeviceIdentifiers[]): Promise<VersionList> {
    if (identifiers.length < 1) {
      return {};
    }
    const response = await this.fetchFromFirmwareEndpoint({
      QueryType: QueryType.ListVersions,
      ProtocolVersion: 1,
      Devices: identifiers,
    });

    let versions: ListVersionsResponse;
    try {
      versions = await response.json();
    } catch (err) {
      throw new Error(`Cannot decode server response: ${makeString(err)}`);
    }
    if (!versions.Success) {
      throw new Error(`List versions failure: ${versions.Message ?? 'no message'}`);
    }
    return _.mapValues(versions.Versions,
      versions => versions.map(
        (version): Version => ({
          str: version.FWVersion,
          parsed: toFirmwareVersion(version),
          notes: version.Notes,
        })
      )
    );
  }

  public async reportInstall(identifiers: QueryFirmwareVersion[]): Promise<void> {
    const response = await this.fetchFromFirmwareEndpoint({
      QueryType: QueryType.ReportInstall,
      ProtocolVersion: 1,
      Devices: identifiers,
    });

    let responseData: QueryResponse;
    try {
      responseData = await response.json();
    } catch (err) {
      throw new Error(`Cannot decode server response: ${makeString(err)}`);
    }
    if (!responseData.Success) {
      throw new Error(`Report install failure: ${responseData.Message ?? 'no message'}`);
    }
  }

  private async saveUpdatePackageToFile(stream: ReadableStream<Uint8Array>, filename: string) {
    const file = path.join(os.tmpdir(), filename);
    const fileHandle = await fs.promises.open(file, 'w');
    try {
      const reader = stream.getReader();
      let result = await reader.read();
      while (!result.done) {
        await fileHandle.appendFile(result.value);
        result = await reader.read();
      }
    } finally {
      await fileHandle.close();
    }
    return file;
  }

  private async fetchUpdatePackageStream(targetVersion: string, targetBuild: number, identifiers: DeviceIdentifiers) {
    const response = await this.fetchFromFirmwareEndpoint({
      QueryType: QueryType.Package,
      ProtocolVersion: 1,
      Versions: [{
        ...identifiers,
        FWVersion: targetVersion,
        BuildNumber: targetBuild,
      }],
    });
    if (!response.ok || response.headers.get('content-type') !== 'application/octet-stream') {
      throw Error([
        `Could not fetch from build ${targetVersion}.${targetBuild} from ${this.baseUrl}:`,
        `Status: ${response.statusText}, ${await response.text()}.`, `Response head: ${response.headers.get('content-type')}`,
      ].join(' '));
    }
    return response.body!;
  }

  public async getUpdatePackage(version: Version, identifiers: DeviceIdentifiers): Promise<string> {
    // Filename is the serial number, followed by the version id with . and whitespace converted to underscore, followed by .fwu
    const filename = `${identifiers.SerialNumber}_${version.str.replace(/[.\s]/g, '_')}.fwu`;
    const stream = await this.fetchUpdatePackageStream(version.str, version.parsed.build, identifiers);
    return await this.saveUpdatePackageToFile(stream, filename);
  }
}

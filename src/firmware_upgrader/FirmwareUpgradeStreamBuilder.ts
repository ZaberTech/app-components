import * as fs from 'fs';

import { DeviceIdentifiers } from './types';


enum LogicalOp {
  AND,
  OR,
  XOR,
}

/**
 * An implementation of the byte stream generation described in https://www.zaber.com/w/Firmware_Upgrade
 */
export class FirmwareUpgradeStreamBuilder {
  private fileHandle?: fs.promises.FileHandle;
  private bytesRead: number = 0;
  private stream?: Uint8Array;
  private identifiers: DeviceIdentifiers;
  private fwuFile: string;
  private registers: Uint8Array;
  private instructionsToSkip: number = 0;

  /**
  * @param identifiers the identifiers of the device to upgrade, used to control what firmware it gets
  * @param file the full path to the fwu file to upgrade with
  */
  constructor(identifiers: DeviceIdentifiers, file: string) {
    this.fwuFile = file;
    this.identifiers = identifiers;
    this.registers = new Uint8Array(Math.pow(2, 16)); // faking a 16 bit register
  }

  /**
   * Reads an FWU file, converting it into a byte stream
   * @returns the bytes stream created
   */
  async buildStream(): Promise<Uint8Array> {
    const realFileLen = (await fs.promises.stat(this.fwuFile)).size;
    this.stream = new Uint8Array();
    this.bytesRead = 0;
    this.instructionsToSkip = 0;
    this.fileHandle = await fs.promises.open(this.fwuFile, 'r');
    try {
      const header = await this.readToUTF8(8);
      if (header !== 'ZABERFWU') {
        throw new Error('Incorrect header. This file does not appear to be a zaber firmware upgrade');
      }
      const protocolVersion = await this.readInt(1);
      if (protocolVersion !== 1) {
        throw new Error(`Unsupported FWU protocol version: ${protocolVersion}`);
      }
      const fileLen = await this.readInt(4);
      if (fileLen !== realFileLen) {
        throw new Error(`FWU header claims file length of ${fileLen} bytes, but real size is ${realFileLen} bytes`);
      }
      while (this.bytesRead < fileLen) {
        await this.executeNextCommand();
      }
      return this.stream;
    } finally {
      await this.fileHandle.close();
    }
  }

  /**
 * Executes a single command from the fwu file
 */
  private async executeNextCommand() {
    const commandByte = await this.readInt(1);
    switch (commandByte) {
      case 0: return await this.logicalOperation(LogicalOp.AND);
      case 1: return await this.logicalOperation(LogicalOp.OR);
      case 2: return await this.logicalOperation(LogicalOp.XOR);
      case 3: return await this.logicalNot();
      case 4: return await this.if();
      case 5: return await this.emitBytes();
      case 6: return await this.error();
      case 7: return await this.isPlatform();
      case 8: return await this.isSerial();
      default: throw new Error(`Unrecognized FWU command: ${commandByte}`);
    }
  }

  private async readFromFile(bytes: number) {
    const buf = Buffer.alloc(bytes);
    const readResult = await this.fileHandle!.read(buf, 0, bytes);
    if (readResult.bytesRead !== bytes) {
      throw new Error(`Attempt to read ${bytes}b from ${this.fwuFile} failed. Read ${readResult.bytesRead}b.`);
    }
    this.bytesRead += bytes;
    return buf;
  }

  private async readToUTF8(bytes: number): Promise<string> {
    const buf = await this.readFromFile(bytes);
    return buf.toString('utf8');
  }

  private async readBytes(bytes: number): Promise<Buffer> {
    const buf = await this.readFromFile(bytes);
    return buf;
  }

  private async readInt(bytes: number): Promise<number> {
    const buf = await this.readFromFile(bytes);
    return buf.readUIntLE(0, bytes);
  }

  private doIfNotSkipping(sideEffects: () => void) {
    if (this.instructionsToSkip > 0) {
      this.instructionsToSkip--;
      return;
    }
    sideEffects();
  }

  private async logicalOperation(op: LogicalOp) {
    const readAddress1 = await this.readInt(2);
    const readAddress2 = await this.readInt(2);
    const writeAddress = await this.readInt(2);
    this.doIfNotSkipping(() => {
      const value1 = this.registers[readAddress1];
      const value2 = this.registers[readAddress2];
      const result = (() => {
        switch (op) {
          case LogicalOp.AND: return value1 & value2;
          case LogicalOp.OR: return value1 | value2;
          case LogicalOp.XOR: return value1 ^ value2;
        }
      })();
      this.registers[writeAddress] = result;
    });
  }

  private async logicalNot() {
    const readAddress = await this.readInt(2);
    const writeAddress = await this.readInt(2);
    this.doIfNotSkipping(() => {
      const value = this.registers[readAddress];
      this.registers[writeAddress] = value ? 0 : 1;
    });
  }

  private async if() {
    const readAddress = await this.readInt(2);
    const linesToSkip = await this.readInt(1);
    this.doIfNotSkipping(() => {
      const value = this.registers[readAddress];
      if (value === 0) {
        this.instructionsToSkip = linesToSkip;
      }
    });
  }

  private async emitBytes() {
    const bytesToEmit = await this.readInt(2);
    const toEmit = await this.readBytes(bytesToEmit);
    this.doIfNotSkipping(() => {
      const concat = new Uint8Array(this.stream!.length + toEmit.length);
      concat.set(this.stream!, 0);
      concat.set(toEmit, this.stream!.length);
      this.stream = concat;
    });
  }

  private async error() {
    const errorMessageLength = await this.readInt(1);
    const msg = await this.readToUTF8(errorMessageLength);
    this.doIfNotSkipping(() => {
      throw new Error(msg);
    });
  }

  private async isPlatform() {
    const targetPlatform  = await this.readInt(4);
    const writeAddress = await this.readInt(2);
    this.doIfNotSkipping(() => {
      this.registers[writeAddress] = targetPlatform === this.identifiers.PlatformID ? 1 : 0;
    });
  }

  private async isSerial() {
    const targetSerialNumber  = await this.readInt(4);
    const writeAddress = await this.readInt(2);
    this.doIfNotSkipping(() => {
      this.registers[writeAddress] = targetSerialNumber === this.identifiers.SerialNumber ? 1 : 0;
    });
  }
}

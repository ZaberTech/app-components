/* eslint-disable @typescript-eslint/no-this-alias */
import * as fs from 'fs';

import { FirmwareWebQuery } from './firmware_web_query';

let fileHandle: MockFileHandle;
jest.spyOn(fs.promises, 'open').mockImplementation(() => Promise.resolve(new MockFileHandle() as unknown as fs.promises.FileHandle));
class MockFileHandle implements Partial<fs.promises.FileHandle> {
  constructor() { fileHandle = this }
  close = jest.fn();
  appendFile = jest.fn();
}

let fetchSpy: jest.SpyInstance;

function getRequestData(nthCall = 0) {
  const options = fetchSpy.mock.calls[nthCall][1];
  return JSON.parse(options.body as string);
}

let instance: FirmwareWebQuery;

beforeEach(() => {
  fetchSpy = jest.spyOn(global, 'fetch');
  instance = new FirmwareWebQuery();
});

afterEach(() => {
  fetchSpy.mockRestore();
  fileHandle = null!;
});

const IDENTIFIERS = {
  DeviceNumber: 1,
  DeviceID: 1234,
  SerialNumber: 1111,
  IsKeyed: true,
  PlatformID: 4567,
};

describe('getVersions', () => {
  beforeEach(() => {
    fetchSpy.mockImplementation(async () => ({
      ok: true,
      json: async () => ({
        ProtocolVersion: 1,
        Success: true,
        Versions: {
          1: [
            {
              Build: 699,
              FWVersion: '7.3',
              Notes: 'http://zaber-release-notes',
            },
            {
              Build: 1233,
              FWVersion: '7.11',
              Notes: 'http://zaber-release-notes',
            },
            {
              Build: 1234,
              FWVersion: 'unlock',
              Notes: 'Use this to unlock',
            },
          ],
          2: [
            {
              Build: 123,
              FWVersion: '6.2',
              Notes: 'http://zaber-release-notes',
            },
          ]
        },
      }),
    } as Response));
  });

  test('encodes the request and returns versions', async () => {
    const versions = await instance.getVersions([IDENTIFIERS]);
    expect(versions).toEqual({
      1: [
        {
          parsed: { major: 7, minor: 3, build: 699 },
          str: '7.3',
          notes: 'http://zaber-release-notes',
        },
        {
          parsed: { major: 7, minor: 11, build: 1233 },
          str: '7.11',
          notes: 'http://zaber-release-notes',
        },
        {
          parsed: { major: 0, minor: 0, build: 1234 },
          str: 'unlock',
          notes: 'Use this to unlock',
        },
      ],
      2: [
        {
          parsed: { major: 6, minor: 2, build: 123 },
          str: '6.2',
          notes: 'http://zaber-release-notes',
        },
      ]
    });

    expect(getRequestData()).toEqual({
      QueryType: 'ListVersions',
      ProtocolVersion: 1,
      Devices: [IDENTIFIERS],
    });
  });

  test('throws error', async () => {
    fetchSpy.mockResolvedValueOnce({
      ok: true,
      json: async () => ({
        ProtocolVersion: 1,
        Success: false,
        Message: 'No version',
      }),
    });

    await expect(instance.getVersions([IDENTIFIERS])).rejects.toThrow('List versions failure: No version');
  });
});

describe('getUpdatePackage', () => {
  beforeEach(() => {
    let readerAt: number;
    const readResults = [
      { value: Buffer.from('DATA1'), done: false },
      { value: Buffer.from('DATA2'), done: false },
      { done: true },
    ];

    fetchSpy.mockImplementation(async () => ({
      ok: true,
      headers: new Headers([
        ['content-type', 'application/octet-stream'],
      ]),
      body: {
        getReader: () => {
          readerAt = 0;
          return {
            read: async () => {
              const result = readResults[readerAt++];
              return result;
            }
          };
        }
      }
    } as Response));
  });

  test('saves the package', async () => {
    const file = await instance.getUpdatePackage({
      parsed: { major: 7, minor: 3, build: 699 },
      str: '7.3',
    }, IDENTIFIERS);
    expect(file).toMatch(/1111_7_3\.fwu$/);
    expect(fileHandle.appendFile.mock.calls.map(call => call[0].toString('ascii'))).toEqual(['DATA1', 'DATA2']);
  });

  test('throws an error if headers are not octet stream', async () => {
    fetchSpy.mockImplementationOnce(async () => ({
      ok: true,
      headers: new Headers(),
      text: async () => 'Some terrible error',
      statusText: 'OK',
    }));

    await expect(instance.getUpdatePackage({
      parsed: { major: 7, minor: 3, build: 699 }, str: '7.3',
    }, IDENTIFIERS)).rejects.toThrowError([
      'Could not fetch from build 7.3.699 from https://www.zaber.com/support/firmware-self-serve.php:',
      'Status: OK, Some terrible error.',
    ].join(' '));
  });
});

describe('reportInstall', () => {
  beforeEach(() => {
    fetchSpy.mockImplementation(async () => ({
      ok: true,
      json: async () => ({
        ProtocolVersion: 1,
        Success: true,
      }),
    } as Response));
  });

  test('reports success', async () => {
    await instance.reportInstall([{
      ...IDENTIFIERS,
      FWVersion: '7.23',
      BuildNumber: 1234,
    }]);

    expect(fetchSpy).toHaveBeenCalled();
    expect(getRequestData()).toEqual({
      QueryType: 'ReportInstall',
      ProtocolVersion: 1,
      Devices: [{
        ...IDENTIFIERS,
        FWVersion: '7.23',
        BuildNumber: 1234,
      }],
    });
  });
});

import * as fs from 'fs';

import { FirmwareUpgradeStreamBuilder } from './FirmwareUpgradeStreamBuilder';

const MOCK_PROTOCOL_VERSION = 1;

const MOCK_SERIAL_NUMBER = 123456;
const MOCK_PLATFORM_ID = 987343;

const MOCK_EMIT_1 = 'SEND';
const MOCK_EMIT_2 = 'TO';
const MOCK_EMIT_3 = 'STREAM';
const MOCK_ERROR_MESSAGE = 'Error parsing Fwu';

function bufferOfInt(int: number, bytes: number) {
  const buf = Buffer.alloc(bytes);
  buf.writeUIntLE(int, 0, bytes);
  return buf;
}

const IS_PLATFORM_BIT = 1;
const IS_SERIAL_BIT = 2;
const XOR_RESULT_BIT = 3;
const NOT_PLATFORM_BIT = 4;
const OR_RESULT_BIT = 5;
const AND_RESULT_BIT = 5;

let useBuffer: number;
let buffers: Buffer[];
let mockFileLength: number;
function setBuffers(replaceIndex?: number, replacementBuffer?: Buffer) {
  buffers = [
    // Header
    Buffer.from('ZABERFWU'),
    bufferOfInt(MOCK_PROTOCOL_VERSION, 1),
    bufferOfInt(0, 4),
    // Emit 1
    bufferOfInt(5, 1),
    bufferOfInt(MOCK_EMIT_1.length, 2),
    Buffer.from(MOCK_EMIT_1),
    // is platform = 1
    bufferOfInt(7, 1),
    bufferOfInt(MOCK_PLATFORM_ID, 4),
    bufferOfInt(IS_PLATFORM_BIT, 2),
    // is serial = 1
    bufferOfInt(8, 1),
    bufferOfInt(MOCK_SERIAL_NUMBER, 4),
    bufferOfInt(IS_SERIAL_BIT, 2),
    // not platform = 0
    bufferOfInt(3, 1),
    bufferOfInt(IS_PLATFORM_BIT, 2),
    bufferOfInt(NOT_PLATFORM_BIT, 2),
    // platform ^ serial = 1 ^ 1 = 0
    bufferOfInt(2, 1),
    bufferOfInt(IS_PLATFORM_BIT, 2),
    bufferOfInt(IS_SERIAL_BIT, 2),
    bufferOfInt(XOR_RESULT_BIT, 2),
    // if (xor result) = if (0)
    bufferOfInt(4, 1),
    bufferOfInt(XOR_RESULT_BIT, 2),
    bufferOfInt(1, 1), // skip one line
    // Error message
    bufferOfInt(6, 1),
    bufferOfInt(MOCK_ERROR_MESSAGE.length, 1),
    Buffer.from(MOCK_ERROR_MESSAGE),
    // !platform | serial = 0 | 1 = 1
    bufferOfInt(1, 1),
    bufferOfInt(NOT_PLATFORM_BIT, 2),
    bufferOfInt(IS_SERIAL_BIT, 2),
    bufferOfInt(OR_RESULT_BIT, 2),
    // if (or result) = if (1)
    bufferOfInt(4, 1),
    bufferOfInt(OR_RESULT_BIT, 2),
    bufferOfInt(1, 1),
    // Emit 2
    bufferOfInt(5, 1),
    bufferOfInt(MOCK_EMIT_2.length, 2),
    Buffer.from(MOCK_EMIT_2),
    // platform & serial = 1 & 1 = 1
    bufferOfInt(0, 1),
    bufferOfInt(IS_PLATFORM_BIT, 2),
    bufferOfInt(IS_SERIAL_BIT, 2),
    bufferOfInt(AND_RESULT_BIT, 2),
    // if (or result) = if (1)
    bufferOfInt(4, 1),
    bufferOfInt(AND_RESULT_BIT, 2),
    bufferOfInt(1, 1),
    // Emit 2
    bufferOfInt(5, 1),
    bufferOfInt(MOCK_EMIT_3.length, 2),
    Buffer.from(MOCK_EMIT_3),
  ];
  if (replaceIndex != null) {
    buffers[replaceIndex] = replacementBuffer!;
  }
  mockFileLength = 0;
  for (const buffer of buffers) {
    mockFileLength += buffer.length;
  }
  // backfill the file length if it's not specifically overwritten
  if (replaceIndex !== 2) {
    buffers[2] = bufferOfInt(mockFileLength, 4);
  }
}

jest.spyOn(fs.promises, 'stat').mockImplementation(() => Promise.resolve({ size: mockFileLength } as fs.Stats));
jest.spyOn(fs.promises, 'open').mockImplementation(() => Promise.resolve(new MockFileHandle() as unknown as fs.promises.FileHandle));

class MockFileHandle implements Partial<fs.promises.FileHandle> {
  async read<T extends NodeJS.ArrayBufferView>(
    writeToBuffer: T,
    offset?: any,
  ): Promise<fs.promises.FileReadResult<T>> {
    const writeIn = buffers[useBuffer++];
    (writeToBuffer as Uint8Array).set(writeIn, offset ?? 0);
    return {
      bytesRead: writeIn.length,
      buffer: writeToBuffer,
    };
  }
  close = jest.fn();
}

let instance: FirmwareUpgradeStreamBuilder;

beforeEach(() => {
  useBuffer = 0;
  setBuffers();

  instance = new FirmwareUpgradeStreamBuilder({
    DeviceID: 1111,
    DeviceNumber: 1,
    IsKeyed: true,
    PlatformID: MOCK_PLATFORM_ID,
    SerialNumber: MOCK_SERIAL_NUMBER,
  }, 'file.bin');
});

afterEach(() => {
  instance = null!;
});

test('testing all the operations, returns data to emit', async () => {
  const stream = await instance.buildStream();
  expect(Buffer.from(stream).toString('ascii')).toBe(MOCK_EMIT_1 + MOCK_EMIT_2 + MOCK_EMIT_3);
});

test('reads wrong length of buffer', async () => {
  setBuffers(0, Buffer.from('ZABER'));
  await expect(instance.buildStream()).rejects.toThrow('Attempt to read 8b from file.bin failed. Read 5b.');
});

test('does not start with ZABERFWU', async () => {
  setBuffers(0, Buffer.from('ZABER :('));
  await expect(instance.buildStream()).rejects.toThrow('Incorrect header. This file does not appear to be a zaber firmware upgrade');
});

test('has unsupported protocol version', async () => {
  setBuffers(1, bufferOfInt(2, 1));
  await expect(instance.buildStream()).rejects.toThrow('Unsupported FWU protocol version: 2');
});

test('has incorrect file length', async () => {
  setBuffers(2, bufferOfInt(0, 4));
  await expect(instance.buildStream()).rejects.toThrow('FWU header claims file length of 0 bytes, but real size is 105 bytes');
});

test('contains unrecognized command', async () => {
  setBuffers(3, bufferOfInt(9, 1));
  await expect(instance.buildStream()).rejects.toThrow('Unrecognized FWU command: 9');
});

test('parsing error', async () => {
  // Change the xor to an or, triggering an error being emitted
  setBuffers(15, bufferOfInt(1, 1));
  await expect(instance.buildStream()).rejects.toThrow('Error parsing Fwu');
});

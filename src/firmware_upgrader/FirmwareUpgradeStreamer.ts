import type { ascii } from '@zaber/motion';

import { FirmwareUpgradeStreamBuilder } from './FirmwareUpgradeStreamBuilder';
import { DeviceIdentifiers } from './types';

function readToUrlSafeBase64(bytes: Uint8Array): string {
  const base64 = Buffer.from(bytes).toString('base64');
  // To match the url safe spec: https://tools.ietf.org/html/rfc4648#section-5
  // replace + with - and / with _
  return base64.replace(/\+/g, '-').replace(/\//g, '_');
}

const LONG_RUNNING_COMMAND_TIMEOUT = { timeout: 10 * 1000 };

export class FirmwareUpgradeStreamer {
  private device: ascii.Device;
  private builder: FirmwareUpgradeStreamBuilder;
  private bytes?: Uint8Array;
  private bytesToWrite: number = 0;
  private bytesWritten: number = 0;

  constructor(device: ascii.Device, identifiers: DeviceIdentifiers, file: string) {
    this.device = device;
    this.builder = new FirmwareUpgradeStreamBuilder(identifiers, file);
  }

  /**
   * @returns The total number of bytes to be streamed
   */
  async beginUpgrade(): Promise<number> {
    this.bytesWritten = 0;
    this.bytes = await this.builder.buildStream();
    const response = await this.device.genericCommand('system upgrade start', LONG_RUNNING_COMMAND_TIMEOUT);
    this.bytesToWrite = parseInt(response.data, 10);
    if (isNaN(this.bytesToWrite)) {
      throw new Error('There was an issue putting the device into bootloader mode to begin upgrade');
    }
    return this.bytes.length;
  }

  /**
   * Streams the next chunk of bytes to the device
   * @returns The number of bytes streamed total or literal 'done'
   */
  async stream(): Promise<number | 'done'> {
    const bytes = this.bytes!.slice(this.bytesWritten, this.bytesWritten + this.bytesToWrite);
    const base64Data = readToUrlSafeBase64(bytes);
    const cmd = `system upgrade data ${base64Data}`;
    const response = await this.device.genericCommand(cmd);
    this.bytesWritten += this.bytesToWrite;
    this.bytesToWrite = parseInt(response.data, 10);
    if (isNaN(this.bytesToWrite)) {
      throw new Error('There was an issue sending update data to device');
    } else if (this.bytesToWrite === 0) {
      await this.device.genericCommand('system upgrade end', LONG_RUNNING_COMMAND_TIMEOUT);
      return 'done';
    } else {
      return this.bytesWritten;
    }
  }
}

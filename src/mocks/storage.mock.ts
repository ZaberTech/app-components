import { injectable, Container } from 'inversify';
import _ from 'lodash';

import { Storage } from '../storage';

@injectable()
export class StorageMock {
  stored: Record<string, unknown> = {};
  load = jest.fn<unknown, [string, unknown]>((key, defaultValue) => _.cloneDeep(this.stored[key] ?? defaultValue));
  save = jest.fn((key, value) => { this.stored[key] = value });
}

export function mockStorage(container: Container): StorageMock {
  container.bind<unknown>(Storage).to(StorageMock);
  return container.get<unknown>(Storage) as StorageMock;
}

import { RecursivePartial } from '@zaber/toolbox';
import _ from 'lodash';

interface ConfigData {
  aws: Partial<{
    region: string;

    cognitoUserPoolId: string;
    cognitoIdentityPoolId: string;

    iotCoreHost: string;
    iotClientIdPrefix: string;
  }>;
  zaber: Partial<{
    apiUrl: string;
    deviceDB: 'public' | 'master' | 'offline';
    firmwareUpgradeServer: 'public' | 'internal';
  }>;
}

export class Config {
  private static _data: ConfigData = {
    aws: {},
    zaber: {},
  };

  static merge(config: RecursivePartial<ConfigData>): void {
    _.merge(Config._data, config);
  }

  static get current(): ConfigData {
    return Config._data;
  }
}

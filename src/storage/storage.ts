import { injectable, inject } from 'inversify';
import _ from 'lodash';

import { Log, Logger } from '../log';

export const LocalStorageSymbol = Symbol('localStorage');

@injectable()
export class Storage {
  private log: Logger;

  constructor(
    @inject(LocalStorageSymbol) private localStorage: WindowLocalStorage['localStorage'],
    @inject(Log) log: Log,
  ) {
    this.log = log.getLogger('storage');
  }

  public save<T>(key: string, value: T | null): void {
    if (value == null) {
      this.localStorage.removeItem(key);
    } else {
      this.localStorage.setItem(key, JSON.stringify(value));
    }
  }

  public load<T>(key: string, defaultValue: T): T;
  public load<T>(key: string): T | null;

  public load<T>(key: string, defaultValue: T | null = null): T | null {
    const raw = this.localStorage.getItem(key);
    if (raw == null) {
      return _.cloneDeep(defaultValue);
    }

    try {
      return JSON.parse(raw) as T;
    } catch (err) {
      this.log.warn(`Cannot deserialize ${key}: ${raw}`, err);
      return _.cloneDeep(defaultValue);
    }
  }

  public clear() {
    this.localStorage.clear();
  }
}

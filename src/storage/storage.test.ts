import 'reflect-metadata';
import { Container, injectable } from 'inversify';

import { createContainer, destroyContainer } from '../test';

import { Storage, LocalStorageSymbol } from './storage';

const TEST_KEY = 'TEST_KEY';

let container: Container;
let storageMock: StorageMock;

@injectable()
class StorageMock {
  public data: Record<string, string | null> = {};

  public setItem = jest.fn((key: string, value: string) => this.data[key] = value);
  public getItem = jest.fn((key: string) => this.data[key] || null);
  public removeItem = jest.fn((key: string) => this.data[key] = null);
  public clear = jest.fn(() => this.data = {});
}

let storage: Storage;

beforeEach(() => {
  container = createContainer();
  container.bind<unknown>(LocalStorageSymbol).to(StorageMock);
  storageMock = (container.get(LocalStorageSymbol) as unknown) as StorageMock;

  storage = container.get(Storage);
});

afterEach(() => {
  storage = null!;

  destroyContainer();
  container = null!;
  storageMock = null!;
});

test('stores provided value', () => {
  storage.save(TEST_KEY, { x: 3 });

  expect(storageMock.data).toEqual({
    TEST_KEY: '{"x":3}',
  });

  const value = storage.load(TEST_KEY);
  expect(value).toEqual({ x: 3 });
});

test('stores string value', () => {
  storage.save(TEST_KEY, 'test_value');
  const value = storage.load(TEST_KEY);
  expect(value).toEqual('test_value');
});

test('returns default value when null is retrieved', () => {
  const value = storage.load(TEST_KEY, 'default');
  expect(value).toBe('default');
});

test('returns default value when value is not JSON parsable', () => {
  storageMock.data[TEST_KEY] = '{';
  const value = storage.load(TEST_KEY, { a: 1 });
  expect(value).toEqual({ a: 1 });
});

test('clears all data', () => {
  storageMock.data[TEST_KEY] = '{';
  storageMock.data[`${TEST_KEY}_2`] = '}';
  storage.clear();
  expect(storageMock.data).toEqual({});
});

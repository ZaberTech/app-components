import { CognitoUser } from 'amazon-cognito-identity-js';
import { take } from 'rxjs/operators';

jest.mock('aws-sdk/lib/credentials/cognito_identity_credentials', () => null);
jest.mock('aws-sdk/lib/core', () => ({
  get CognitoIdentityCredentials(): unknown {
    return CognitoIdentityCredentialsMock;
  },
}));
jest.mock('@aws-amplify/auth', () => ({
  get Auth(): unknown {
    return AmplifyAuthMock;
  },
}));

import { Config } from '../config';

import { AwsCredentialsService } from './credentials';

class CognitoIdentityCredentialsMock {
  constructor(public readonly config: unknown) {}
  getPromise = jest.fn().mockResolvedValue(null);
  needsRefresh = jest.fn().mockReturnValue(false);
  clearCachedId = jest.fn();
}

class UserSessionMock {
  getIdToken = () => ({
    getJwtToken: () => 'jwt-token',
  });
}

class AmplifyAuthMock {
  static userSession = jest.fn(() => new UserSessionMock());
}

let service: AwsCredentialsService;

beforeEach(() => {
  service = new AwsCredentialsService();
});
afterEach(() => {
  service = null!;
});
beforeAll(() => {
  Config.merge({
    aws: {
      cognitoIdentityPoolId: 'identityPoolId',
      cognitoUserPoolId: 'userPoolId',
      region: 'region',
    }
  });
});

describe('getCredentials', () => {
  test('returns anonymous credentials when user is null', async () => {
    await service.setUser('unauthenticated');

    const credentials = await service.getCredentials();
    expect(credentials.authenticated).toBe(false);

    const mock = credentials.cognito as unknown as CognitoIdentityCredentialsMock;
    expect(mock.getPromise).toHaveBeenCalledTimes(1);
    expect(mock.config).toMatchObject({ IdentityPoolId: 'identityPoolId' });
  });

  test('returns user credentials when user is set', async () => {
    await service.setUser('fake-user' as unknown as CognitoUser);

    const credentials = await service.getCredentials();
    expect(credentials.authenticated).toBe(true);

    const mock = credentials.cognito as unknown as CognitoIdentityCredentialsMock;
    expect(mock.getPromise).toHaveBeenCalledTimes(1);
    expect(mock.config).toMatchObject({
      IdentityPoolId: 'identityPoolId',
      Logins: {
        'cognito-idp.region.amazonaws.com/userPoolId': 'jwt-token',
      },
    });

    expect(AmplifyAuthMock.userSession).toHaveBeenCalledWith('fake-user');
  });

  test('returns same credentials if they are valid', async () => {
    await service.setUser('unauthenticated');

    const credentials = await service.getCredentials();
    const credentials2 = await service.getCredentials();
    expect(credentials.cognito).toBe(credentials2.cognito);


    const mock = credentials.cognito as unknown as CognitoIdentityCredentialsMock;
    mock.needsRefresh.mockReturnValueOnce(true);

    const credentials3 = await service.getCredentials();
    expect(credentials3.cognito).not.toBe(credentials);
  });

  test('returns new credentials when user is set (and emits renewCredentials)', async () => {
    await service.setUser('unauthenticated');
    const credentials = await service.getCredentials();

    const emitPromise = service.renewCredentials.pipe(take(1)).toPromise();
    await service.setUser('fake-user' as unknown as CognitoUser);

    await emitPromise;

    const credentialsNew = await service.getCredentials();
    expect(credentials.cognito).not.toBe(credentialsNew.cognito);
  });

  test('waits for set user to resolve credentials', async () => {
    const credentialsPromise1 = service.getCredentials();
    const credentialsPromise2 = service.getCredentials();

    await service.setUser('unauthenticated');

    const credentials1 = await credentialsPromise1;
    const credentials2 = await credentialsPromise2;
    expect(credentials1.cognito).toBe(credentials2.cognito);
  });
});

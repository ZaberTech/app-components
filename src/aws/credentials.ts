import { injectable } from 'inversify';
import { CognitoUser } from 'amazon-cognito-identity-js';
import 'aws-sdk/lib/credentials/cognito_identity_credentials';
import { CognitoIdentityCredentials } from 'aws-sdk/lib/core';
import { BehaviorSubject, Subject, Observable } from 'rxjs';
import { filter, take, timeout } from 'rxjs/operators';
import { Mutex } from 'async-mutex';
import { Auth } from '@aws-amplify/auth';

import { Config } from '../config';

export interface Credentials {
  cognito: CognitoIdentityCredentials;
  authenticated: boolean;
}

type User = CognitoUser | 'unauthenticated';
const SET_USER_TIMEOUT = 10000;

function getUserPoolIdp(): string {
  return `cognito-idp.${Config.current.aws.region}.amazonaws.com/${Config.current.aws.cognitoUserPoolId}`;
}

@injectable()
export class AwsCredentialsService {
  private currentUser = new BehaviorSubject<User | 'userNotSet'>('userNotSet');
  private credentials: Credentials | null = null;

  private readonly lock = new Mutex();

  private _renewCredentials = new Subject<void>();
  public readonly renewCredentials: Observable<void> = this._renewCredentials;

  public async setUser(user: User): Promise<void> {
    const shouldRenew = this.currentUser.value !== 'userNotSet';
    if (shouldRenew) {
      const unlock = await this.lock.acquire();
      const credentials = this.credentials;
      this.credentials = null;
      unlock();

      // aws-sdk caches the identityId from the previous successful credentials fetch and stores it
      // in localStorage on browsers. The cached identity id creates errors when we change users
      // and try to get new credentials because aws-sdk tries to get credentials for that old
      // cached identity id
      credentials?.cognito.clearCachedId();
    }

    this.currentUser.next(user);
    if (shouldRenew) {
      this._renewCredentials.next();
    }
  }

  public async getCredentials(): Promise<Credentials> {
    const unlock = await this.lock.acquire();
    try {
      if (this.credentials && !this.credentials.cognito.needsRefresh()) {
        return this.credentials;
      }
      // CognitoIdentityCredentials cannot be refreshed, we need to obtain new ones

      const currentUser = await this.currentUser.pipe(
        filter(user => user !== 'userNotSet'),
        take(1),
        timeout(SET_USER_TIMEOUT)
      ).toPromise();
      const authenticated = currentUser !== 'unauthenticated';

      const options: CognitoIdentityCredentials.CognitoIdentityOptions = {
        IdentityPoolId: Config.current.aws.cognitoIdentityPoolId ?? '',
      };
      if (authenticated) {
        const userSession = await Auth.userSession(currentUser);
        const idToken = userSession.getIdToken().getJwtToken();
        options.Logins = {
          [getUserPoolIdp()]: idToken,
        };
      }

      const credentials = new CognitoIdentityCredentials(options, { region: Config.current.aws.region });
      await credentials.getPromise();

      this.credentials = { cognito: credentials, authenticated };
      return this.credentials;
    } finally {
      unlock();
    }
  }
}

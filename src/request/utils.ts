import { HttpError } from './http_error';

export interface RequestOptions {
  headers?: Record<string, string>;
  search?: string[][] | Record<string, string> | URLSearchParams;
  formUrlEncoded?: true;
}

interface FetchOptions {
  headers: Record<string, string>;
  body: BodyInit | null | undefined;
  method: string;
}

export function urlWithSearch(url: string, options?: RequestOptions): string {
  if (options?.search) {
    const search = options.search instanceof URLSearchParams ? options.search : new URLSearchParams(options.search);
    return `${url}?${search.toString()}`;
  }
  return url;
}

export function fetchOptions(method?: string | null, body?: unknown, options?: RequestOptions): FetchOptions {
  const headers: Record<string, string> = {
    ...(options?.headers)
  };
  if (body != null) {
    if (options && options.formUrlEncoded === true) {
      headers['Content-Type'] = 'application/x-www-form-urlencoded';
      body = new URLSearchParams(body as Record<string, string>).toString();
    } else if (body instanceof FormData) {
      headers['Content-Type'] = 'multipart/form-data';
    } else {
      headers['Content-Type'] = 'application/json';
      body = JSON.stringify(body);
    }
  }

  return {
    headers,
    body: body as BodyInit,
    method: method || 'GET',
  };
}

export async function parseContent(
  status: number | undefined, contentType: string | null, toText: () => string | Promise<string>, toObj: () => unknown
): Promise<unknown> {
  const isError = status == null || status < 200 || status > 299;

  let content: unknown;
  try {
    if (status === 204) {
      content = null;
    } else if (contentType?.startsWith('application/json')) {
      content = await toObj();
    } else {
      content = await toText();
    }
  } catch (contentError) {
    if (!isError) {
      throw contentError;
    }
  }
  if (isError) {
    throw new HttpError(`Request error status: ${status}`, status, content);
  }
  return content;
}

export class HttpError extends Error {
  constructor(
    message: string,
    public readonly status?: number,
    public readonly result?: unknown,
  ) {
    super(message);
    Object.setPrototypeOf(this, HttpError.prototype);
  }
}

export class ConnectionError extends Error {
  constructor(message: string) {
    super(message);
    Object.setPrototypeOf(this, ConnectionError.prototype);
  }
}

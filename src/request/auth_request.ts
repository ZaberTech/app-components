import { inject, injectable } from 'inversify';
import { Auth } from '@aws-amplify/auth';
import { ensureError } from '@zaber/toolbox';

import { Request } from './request';
import { AuthenticationError } from './auth_error';
import { RequestOptions } from './utils';

@injectable()
export class AuthRequest {
  constructor(
    @inject(Request) private readonly requestService: Request
  ) {
    this.request = this.request.bind(this);
  }

  /**
   * Runs the request with the currently logged in user's authorization token added to the request headers.
   *
   * This is needed for making API calls to endpoints that are protected by a cognito authorizer.
   */
  public async request<Return = unknown>(
    url: string, method?: string | null, body?: unknown, options?: RequestOptions
  ): Promise<Return> {
    let authorization: string;
    try {
      authorization = (await Auth.currentSession()).getAccessToken().getJwtToken();
    } catch (e) {
      throw new AuthenticationError(`Failed to run authenticated API call. ${ensureError(e).message}`);
    }

    return this.requestService.request(url, method, body, {
      ...options,
      headers: {
        ...options?.headers,
        authorization,
      }
    });
  }
}

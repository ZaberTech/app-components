import { injectable } from 'inversify';
import AwsAuth from '@aws-amplify/auth';
import { CognitoAccessToken, CognitoUserSession } from 'amazon-cognito-identity-js';

import { createContainer, destroyContainer } from '../test';

import { Request } from './request';
import { AuthRequest } from './auth_request';
import { AuthenticationError } from './auth_error';

const getJwtTokenMock = jest.fn().mockReturnValue('abcde12345');
(jest.spyOn(AwsAuth, 'currentSession')).mockResolvedValue({
  getAccessToken: () => ({
    getJwtToken: getJwtTokenMock
  } as unknown as CognitoAccessToken),
} as CognitoUserSession);

@injectable()
class RequestMock {
  request = jest.fn().mockResolvedValue({});
}

let request: RequestMock;
let authRequest: AuthRequest;

beforeEach(() => {
  const container = createContainer();
  container.bind<unknown>(Request).to(RequestMock);

  request = container.get<unknown>(Request) as RequestMock;
  authRequest = container.get(AuthRequest);
});

afterEach(() => {
  jest.clearAllMocks();
  destroyContainer();
  request = null!;
  authRequest = null!;
});

test('forwards request parameters and overrides "authorization" header with required value', async () => {
  const url = 'http://test.url.com';
  const method = 'POST';
  const body = { some: 'body', once: 'told me', you: 'need some macaroni' };
  const options = { headers: { authorization: 'blah' } };

  const result = await authRequest.request(url, method, body, options);

  expect(result).toEqual({});
  expect(getJwtTokenMock).toHaveBeenCalledTimes(1);
  expect(request.request).toBeCalledWith(url, method, body, expect.objectContaining({
    headers: expect.objectContaining({
      authorization: 'abcde12345',
    })
  }));
});

test('throws authentication error if current user info cannot be obtained', async () => {
  (AwsAuth.currentSession as unknown as jest.MockInstance<Promise<void>, []>).mockRejectedValue('No current user');
  await expect(authRequest.request('http://test.url.com')).rejects.toThrow(AuthenticationError);
});

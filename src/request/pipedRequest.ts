import http from 'node:http';
import net from 'node:net';

import { urlWithSearch, fetchOptions, parseContent, RequestOptions } from './utils';

export function pipedRequest<Return = unknown>(
  pipePath: string, baseUrl: string, method?: string | null, body?: unknown, requestOptions?: RequestOptions
): Promise<Return> {
  const url = urlWithSearch(baseUrl, requestOptions);
  const options = fetchOptions(method, body, requestOptions);

  return new Promise<Return>((resolve, reject) => {
    const req = http.request({
      path: url,
      createConnection: () => net.createConnection({ path: pipePath }),
      method: options.method,
      headers: options.headers,
    }, response => {
      response.setEncoding('utf8');
      let data = '';
      response.on('data', chunk => {
        data += `${chunk}`;
      });
      response.on('end', async () => {
        try {
          const contentType = response.headers['content-type'] ?? null;
          const content = await parseContent(response.statusCode, contentType, () => data, () => JSON.parse(data));
          resolve(content as Return);
        } catch (err) {
          reject(err as Error);
        }
      });
    });
    req.on('error', reject);
    req.write(JSON.stringify(options.body));
    req.end();
  });
}

import { injectable, inject } from 'inversify';

import { Log, Logger } from '../log';

import { safeFetch } from './fetch';
import { HttpError } from './http_error';
import { fetchOptions, parseContent, RequestOptions, urlWithSearch } from './utils';

@injectable()
export class Request {
  protected log: Logger;

  constructor(@inject(Log) log: Log) {
    this.log = log.getLogger('Requests');

    this.request = this.request.bind(this);
  }

  public async request<Return = unknown>(
    baseUrl: string, method?: string | null, body?: unknown, requestOptions?: RequestOptions
  ): Promise<Return> {
    const url = urlWithSearch(baseUrl, requestOptions);
    const options = fetchOptions(method, body, requestOptions);
    const response = await safeFetch(url, options);
    const { status } = response;
    this.log.info(options.method, url, `-> ${status}`);

    try {
      const content = await parseContent(status, response.headers.get('Content-Type'), () => response.text(), () => response.json());
      return content as Return;
    } catch (e) {
      if (e instanceof HttpError) {
        this.log.warn(e.message, e.result);
      }
      throw e;
    }
  }
}

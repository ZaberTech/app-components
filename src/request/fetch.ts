import { ConnectionError } from './http_error';

/**
 * Improves error handling of fetch.
 * Throws ConnectionError instead of TypeError.
 */
export function safeFetch(...args: Parameters<typeof fetch>): ReturnType<typeof fetch> {
  return fetch(...args).catch(err => {
    if (err instanceof TypeError) {
      throw new ConnectionError(err.message);
    }
    throw err;
  });
}

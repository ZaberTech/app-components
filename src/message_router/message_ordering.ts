import { Observable } from 'rxjs';
import _ from 'lodash';

export function ensureMessageOrder<T extends { id: number }>(deliveryTimeout: number = 10 * 1000, firstID = 0) {
  return function(source: Observable<T>): Observable<T> {
    return new Observable(subscriber => {
      const nextID = new Uint32Array([firstID]);
      const register = new Int32Array([0]);
      const storedMessages: T[] = [];
      let timeout: ReturnType<typeof setTimeout> | null = null;

      const processStored = () => {
        storedMessages.sort((a, b) => {
          register[0] = b.id;
          register[0] -= a.id;
          return register[0];
        });

        while (_.last(storedMessages)?.id === nextID[0]) {
          const oldest = storedMessages.pop()!;
          nextID[0] += 1;
          subscriber.next(oldest);
        }

        if (storedMessages.length > 0 && timeout == null) {
          timeout = setTimeout(
            () => subscriber.error(new Error('Missing message caused timeout')),
            deliveryTimeout,
          );
        } else if (storedMessages.length === 0 && timeout != null) {
          clearTimeout(timeout);
          timeout = null;
        }
      };

      const observer = source.subscribe({
        next(message) {
          register[0] = message.id;
          register[0] -= nextID[0];
          const idDiff = register[0];

          if (idDiff === 0) {
            nextID[0] += 1;
            subscriber.next(message);
            processStored();
          } else if (idDiff > 0) {
            storedMessages.push(message);
            processStored();
          } else {
            return;
          }
        },
        error(error) {
          subscriber.error(error);
        },
        complete() {
          subscriber.complete();
        }
      });

      return () => {
        if (timeout != null) {
          clearTimeout(timeout);
          timeout = null;
        }
        observer.unsubscribe();
      };
    });
  };
}

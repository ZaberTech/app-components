import { Subject } from 'rxjs';
import { share, map, toArray } from 'rxjs/operators';

import { ensureMessageOrder } from './message_ordering';

jest.useFakeTimers();

interface Message { id: number; reply: string }

function getReply() {
  return map<Message, string>(message => message.reply);
}

describe('message ordering operator', () => {
  test('handles out of order delivery and duplication', async () => {
    const subject = new Subject<Message>();
    const replies = subject.pipe(ensureMessageOrder(), getReply(), toArray()).toPromise();

    subject.next({ id: 1, reply: '1' });
    subject.next({ id: 0, reply: '0' });
    subject.next({ id: 2, reply: '2' });
    subject.next({ id: 5, reply: '5' });
    subject.next({ id: 2, reply: '2' });
    subject.next({ id: 4, reply: '4' });
    subject.next({ id: 3, reply: '3' });
    subject.complete();

    expect(await replies).toEqual(['0', '1', '2', '3', '4', '5']);
  });

  test('errors when missing message is not received in timeout', async () => {
    const subject = new Subject<Message>();
    const observable = subject.pipe(ensureMessageOrder(), getReply()).pipe(share());

    const values: string[] = [];
    observable.subscribe(value => values.push(value), () => null);

    const errorPromise = observable.toPromise();

    subject.next({ id: 1, reply: '1' });
    subject.next({ id: 0, reply: '0' });
    subject.next({ id: 2, reply: '2' });
    jest.runAllTimers(); // no timers should exist at this moment

    subject.next({ id: 3, reply: '3' });
    subject.next({ id: 5, reply: '5' });
    jest.runAllTimers();

    await expect(errorPromise).rejects.toEqual(expect.objectContaining({ message: 'Missing message caused timeout' }));
    expect(values).toEqual(['0', '1', '2', '3']);
  });

  test('propagates error', () => {
    const subject = new Subject<Message>();
    let err: Error | undefined;
    subject.pipe(ensureMessageOrder()).subscribe({ error: e => err = e });
    subject.error(new Error('broken'));
    expect(err?.message).toBe('broken');
  });

  test('works correctly when message ID overflows', async () => {
    const subject = new Subject<Message>();
    const replies = subject.pipe(ensureMessageOrder(1000, 4294967293), getReply(), toArray()).toPromise();

    subject.next({ id: 4294967294, reply: '-2' });
    subject.next({ id: 1, reply: '1' });
    subject.next({ id: 4294967293, reply: '-3' });
    subject.next({ id: 4294967293, reply: '-3' });
    subject.next({ id: 0, reply: '0' });
    subject.next({ id: 4294967295, reply: '-1' });
    subject.next({ id: 2, reply: '2' });
    subject.complete();

    expect(await replies).toEqual(['-3', '-2', '-1', '0', '1', '2']);
  });
});

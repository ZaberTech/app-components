export const MIN_REQUIRED_API_VERSION = '1.0';
export const CLIENT_VERSION = '2.0';

export interface SerialPortConfig {
  serialPort: string;
  name: string;
  baudRate?: number | null;
}
export interface RawTcpConfig {
  hostname: string;
  name: string;
  tcpPort: number;
}
export enum ConnectionType {
  TCP_IP = 'tcp/ip',
  SERIAL_PORT = 'serial',
}
export interface Connection<TConfig extends SerialPortConfig | RawTcpConfig = SerialPortConfig | RawTcpConfig> {
  id: string;
  type: ConnectionType;
  config: TConfig;
}

export function isConnectionSerialPort(connection: Connection): connection is Connection<SerialPortConfig> {
  return connection.type === ConnectionType.SERIAL_PORT;
}
export function isConnectionTcpIp(connection: Connection): connection is Connection<RawTcpConfig> {
  return connection.type === ConnectionType.TCP_IP;
}

export interface CloudConfig {
  id: string;
  apiUrl: string;
  realm: string;
  token: string;
}

export interface CloudStatus {
  isConnected: boolean;
}

export interface LocalShareConfig {
  enabled: boolean;
  port: number;
  discoverable: boolean;
  instanceName: string;
}

export interface LocalShareStatus {
  isRunning: boolean;
  lastRunError?: string;
  isDiscoverable: boolean;
  lastDiscoverabilityError?: string;
}

export type MonitorMessageSource = 'cmd' | 'cmd-other' | 'reply' | 'reply-other' | 'broadcast';
export interface MonitorMessage {
  message: string;
  source: MonitorMessageSource;
}

export interface RoutedConnectionOptions {
  defaultTimeout?: number;
}

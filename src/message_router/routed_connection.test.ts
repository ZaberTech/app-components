/* eslint-disable @typescript-eslint/no-this-alias */

import { Observable, Subject, Subscriber } from 'rxjs';
import { share } from 'rxjs/operators';
import { ConnectionFailedException, MotionLibException } from '@zaber/motion';

import { waitUntilPass, defer } from '../test';

import { RoutedConnection } from './routed_connection';
import { RpcClient, RpcError, RpcNotifications } from './rpc';

const REQUEST_TIMEOUT = 501;

let connection: RoutedConnection;
let notificationMock: NotificationMock;

class NotificationMock {
  public subscribers = new Map<string, Subscriber<(string|number|string[])[]>>();
  constructor() {
    notificationMock = this;
  }

  getNotifications = jest.fn((method: string) => new Observable<(string|number|string[])[]>(subscriber => {
    this.subscribers.set(method, subscriber);
    return () => { this.subscribers.delete(method) };
  }).pipe(share()));

  reply(line: string, id: number): void {
    this.subscribers.get('routing_reply')!.next(['connection_id', [line], id]);
  }
  error(error: string): void {
    this.subscribers.get('routing_error')!.next(['connection_id', error]);
  }
}

let rpcMock: RpcMock;

class RpcMock {
  constructor() {
    rpcMock = this;
  }

  call = jest.fn(() => Promise.resolve());
  notify = jest.fn(() => Promise.resolve());
  ended = new Subject<true>();
}

beforeEach(() => {
  connection = new RoutedConnection(
    'connection_id',
    (new RpcMock() as unknown) as RpcClient,
    (new NotificationMock() as unknown) as RpcNotifications,
    { defaultTimeout: REQUEST_TIMEOUT },
  );
});

afterEach(async () => {
  await connection.close();
  connection = null!;

  notificationMock = null!;
  rpcMock = null!;
});

describe('open', () => {
  test('calls warmup on open and subscribes', async () => {
    await connection.ensureOpen();

    expect(notificationMock.getNotifications).toBeCalledTimes(2);
    expect(notificationMock.getNotifications.mock.calls[0][0]).toBe('routing_reply');
    expect(notificationMock.getNotifications.mock.calls[1][0]).toBe('routing_error');

    expect(rpcMock.call).toBeCalledWith('routing_warmup', 'connection_id');
  });

  test('multiple opens result in one warmup call and both wait for it', async () => {
    const deferred = defer<void>();
    rpcMock.call.mockImplementationOnce(() => deferred.promise);

    let resolved = false;
    const promise1 = connection.ensureOpen().then(() => resolved = true);
    const promise2 = connection.ensureOpen().then(() => resolved = true);

    expect(rpcMock.call).toBeCalledTimes(1);
    expect(resolved).toBe(false);

    deferred.resolve();

    await Promise.all([promise1, promise2]);
  });

  test('ends connection when warmup call fails', async () => {
    rpcMock.call.mockRejectedValueOnce(new RpcError(0, 'Failure'));

    const promise = connection.ensureOpen();
    await expect(promise).rejects.toBeInstanceOf(ConnectionFailedException);

    await connection.ended.toPromise();
  });
});

describe('opened', () => {
  beforeEach(async () => {
    await connection.ensureOpen();
  });

  test('sets default request timeout', () => {
    const asciiConnection = connection.connection;
    expect(asciiConnection.defaultRequestTimeout).toBe(REQUEST_TIMEOUT);
  });

  test('routes messages from the ASCII connection', async () => {
    const asciiConnection = connection.connection;
    const commandPromise = asciiConnection.genericCommand('get pos', { device: 2 });

    await waitUntilPass(() => expect(rpcMock.notify).toBeCalledTimes(1), 500);
    expect(rpcMock.notify.mock.calls[0]).toEqual(['routing_sendMessage', 'connection_id', '/2 0 00 get pos:2C', 0]);

    notificationMock.reply('@02 0 00 OK IDLE -- 123', 0);

    const { data } = await commandPromise;
    expect(data).toBe('123');
  });

  test('unsubscribes at end and emits end', async () => {
    const endEmitPromise = connection.ended.toPromise();

    await connection.close();

    await endEmitPromise;
    expect(notificationMock.subscribers.size).toBe(0);
  });

  test('closes the connection when rpc send fails', async () => {
    const asciiConnection = connection.connection;

    rpcMock.notify.mockImplementationOnce(() => Promise.reject(new RpcError(1, 'Cannot send request')));

    const commandPromise = asciiConnection.genericCommand('get pos');

    const error: MotionLibException = await commandPromise.catch(e => e);
    expect(error).toBeInstanceOf(MotionLibException);
    expect(error.message).toContain('Cannot send request');

    await connection.ended.toPromise();
  });

  test('closes the connection when rpc ends', async () => {
    rpcMock.ended.next(true);
    rpcMock.ended.complete();

    await connection.ended.toPromise();
  });

  test('closes the connection on notification of error', async () => {
    notificationMock.error('Serial port broken');

    await connection.ended.toPromise();
  });

  test('cooldowns the connection after closing', async () => {
    await connection.close();

    await waitUntilPass(() =>
      expect(rpcMock.call).toHaveBeenLastCalledWith('routing_cooldown', 'connection_id')
    );
  });
});

/* eslint-disable @typescript-eslint/no-this-alias */

import { injectable } from 'inversify';
import { Observable, Subject } from 'rxjs';

import { createContainer, destroyContainer } from '../../test/container';
import { IoTService, Message } from '../../iot';

import { RpcIoIoT } from './rpc_io_iot';

let iotMock: IoTMock;

@injectable()
class IoTMock {
  readonly clientId = 'client1';

  constructor() {
    iotMock = this;
  }

  getConnection = jest.fn().mockImplementation(() => Promise.resolve(this.connection));

  messages = new Subject<Message>();
  connection = {
    clientId: 'client1',
    subscribe: jest.fn<Promise<Observable<Message>>, [string]>(async () => this.messages),
    publish: jest.fn<void, [string, string]>(),
  };
}

let io: RpcIoIoT;
let linesOut: string[];
let iotService: IoTService;

beforeEach(() => {
  const container = createContainer();
  container.bind<unknown>(IoTService).to(IoTMock);
  iotService = container.get(IoTService);

  io = new RpcIoIoT(iotService, {
    realm: 'realm1',
    routerId: 'router1',
  });
  linesOut = [];
  io.setLineCallback(line => linesOut.push(line));
});

afterEach(() => {
  destroyContainer();

  io = null!;
  linesOut = null!;
  iotMock = null!;
});

test('subscribes to correct topic', async () => {
  await io.connect();

  expect(iotMock.connection.subscribe).toHaveBeenCalled();
  expect(iotMock.connection.subscribe.mock.calls[0]).toMatchObject([
    'realm1/message-router/client1/router1/rpc',
  ]);
});

test('emits error and rejects if initialization fails', async () => {
  let error: Error | undefined;
  io.errors.subscribe(err => error = err);
  iotMock.connection.subscribe.mockRejectedValueOnce(new Error('Cloud down'));

  const promise = io.connect();
  await expect(promise).rejects.toMatchObject({ message: 'Cloud down' });

  expect(error?.message).toBe('Cloud down');
});

describe('connected', () => {
  beforeEach(async () => {
    await io.connect();
  });

  test('forwards messages from cloud', () => {
    iotMock.messages.next({
      topic: '',
      payload: 'data1',
    });
    iotMock.messages.next({
      topic: '',
      payload: 'data2',
    });
    expect(linesOut).toMatchObject(['data1', 'data2']);
  });

  test('publishes message to cloud', async () => {
    await io.pushLine('data1');
    await io.pushLine('data2');

    expect(iotMock.connection.publish).toHaveBeenCalledTimes(2);
    expect(iotMock.connection.publish.mock.calls[0]).toMatchObject([
      'realm1/message-router/router1/client1/rpc',
      'data1',
    ]);
    expect(iotMock.connection.publish.mock.calls[1]).toMatchObject([
      'realm1/message-router/router1/client1/rpc',
      'data2',
    ]);
  });

  test('emits error on cloud error', () => {
    let error: Error | undefined;
    io.errors.subscribe(err => error = err);

    iotMock.messages.error(new Error('Cloud disconnected'));

    expect(error?.message).toBe('Cloud disconnected');
  });

  test('updates last message timestamp', async () => {
    await io.pushLine('data1');

    expect(io.lastMessageTs > 0).toBe(true);
  });

  test('unsubscribes at close', async () => {
    await io.close();
    expect(iotMock.messages.observers).toHaveLength(0);
  });

  test('completes error on close', async () => {
    let completed = false;
    io.errors.subscribe({
      complete: () => completed = true,
    });
    await io.close();
    expect(completed).toBe(true);
  });
});

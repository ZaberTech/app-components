import { Observable, Subject } from 'rxjs';
import _ from 'lodash';

import { RpcClient } from './rpc_client';

// eslint-disable-next-line @typescript-eslint/no-explicit-any
type NotificationData = any[];

interface MethodInfo {
  method: string;
  args: string[];
  subject: Subject<NotificationData>;
}

export class RpcNotifications {
  private methods = new Map<string, MethodInfo>();

  private ended = false;

  constructor(private readonly client: RpcClient) {
    client.ended.subscribe(this.onEnd.bind(this));
  }

  private onEnd(): void {
    this.ended = true;

    for (const { subject } of this.methods.values()) {
      subject.complete();
    }
  }

  private async onNotification(info: MethodInfo, ...args: NotificationData): Promise<void> {
    info.subject.next(args);
  }

  public getNotifications<T extends NotificationData>(method: string, args: string[]): Observable<T> {
    if (this.ended) {
      throw new Error('Underlying client has ended');
    }

    let info = this.methods.get(method);
    if (info) {
      if (!_.isEqual(info.args, args)) {
        throw new Error(`Args mismatch: ${args.join()} !== ${info.args.join()}`);
      }
    } else {
      info = {
        method,
        args,
        subject: new Subject(),
      };
      this.methods.set(method, info);
      this.client.addMethod(method, args, this.onNotification.bind(this, info));
    }

    return info.subject as unknown as Observable<T>;
  }

  public hasSubscriptions(): boolean {
    return Array.from(this.methods.values()).some(method => method.subject.observers.length > 0);
  }
}

import { take } from 'rxjs/operators';

import { defer, waitTick } from '../../test';

import { RpcClient } from './rpc_client';
import { Request, Response, ErrorCodes } from './types';
import { RpcError } from './rpc_error';

jest.useFakeTimers();

class IoMock {
  public readonly outgoing: (Request | Response)[] = [];
  private callback?: (line: string) => void;
  private waitForOutgoing?: (() => void) | null = null;
  public isReliable = false;

  public async pushLine(line: string): Promise<void> {
    this.outgoing.push(JSON.parse(line));

    if (this.waitForOutgoing) {
      this.waitForOutgoing();
      this.waitForOutgoing = null;
    }
  }
  public setLineCallback(callback: (line: string) => void): void {
    this.callback = callback;
  }
  public mockIncoming(packet: Request | Response): void {
    this.callback!(JSON.stringify(packet));
  }
  public mockIncomingInvalid(packet: string): void {
    this.callback!(packet);
  }
  public getOutgoing(): (Request | Response | null) {
    return this.outgoing.shift() ?? null;
  }
  public async waitOutgoing(): Promise<Request | Response> {
    if (this.outgoing.length === 0) {
      await new Promise<void>(resolve => this.waitForOutgoing = resolve);
    }
    return this.getOutgoing()!;
  }
}

const DEFAULT_TIMEOUT = 3000;

let client: RpcClient;
let ioMock: IoMock;
let errors: unknown[];

let currentTimestamp = 0;

beforeEach(() => {
  ioMock = new IoMock();
  client = new RpcClient(ioMock, { now: () => currentTimestamp }, { timeout: DEFAULT_TIMEOUT });

  errors = [];
  client.errors.subscribe(err => errors.push(err));
});

afterEach(() => {
  expect(errors).toHaveLength(0);
  expect(ioMock.outgoing).toHaveLength(0);

  client.end();
});

describe('requests', () => {
  test('handles requests', async () => {
    const request1 = client.call('solve', 'y=x*3', { x: 3 });
    const request2 = client.call('sort', [2, 3, 1], 'asc');

    expect(ioMock.getOutgoing()).toEqual({
      id: 1,
      jsonrpc: '2.0',
      method: 'solve',
      params: [
        'y=x*3',
        { x: 3 },
      ],
    });
    expect(ioMock.getOutgoing()).toEqual({
      id: 2,
      jsonrpc: '2.0',
      method: 'sort',
      params: [
        [2, 3, 1],
        'asc',
      ],
    });

    ioMock.mockIncoming({ jsonrpc: '2.0', id: 2, result: [1, 2, 3] });
    ioMock.mockIncoming({ jsonrpc: '2.0', id: 1, result: { y: 9 } });

    const [result1, result2] = await Promise.all([request1, request2]);

    expect(result1).toEqual({ y: 9 });
    expect(result2).toEqual([1, 2, 3]);
  });

  test('handles errors', async () => {
    const request = client.call('sort', [2, 3, 1, 'a']);

    expect(ioMock.getOutgoing()).toMatchObject({
      id: 1,
      method: 'sort',
    });

    ioMock.mockIncoming({
      jsonrpc: '2.0',
      id: 1,
      error: {
        code: ErrorCodes.InvalidParams,
        message: 'The array does not contain only numbers',
        data: { position: 3 },
      }
    });

    try {
      await request;
      throw new Error('Error not thrown');
    } catch (err) {
      expect(err).toBeInstanceOf(RpcError);
      const rpcErr = err as RpcError;
      expect(rpcErr).toMatchObject({
        code: ErrorCodes.InvalidParams,
        message: 'The array does not contain only numbers',
        data: { position: 3 },
      });
    }
  });

  test('ignores reply with unknown id', async () => {
    ioMock.mockIncoming({ jsonrpc: '2.0', id: 2, result: [1, 2, 3] });

    await waitTick();
    expect(ioMock.getOutgoing()).toBe(null);
  });
});

describe('notifications', () => {
  test('sends notification', async () => {
    await client.notify('melt', { marshmallows: 2 });
    expect(ioMock.getOutgoing()).toEqual({
      jsonrpc: '2.0',
      method: 'melt',
      params: [
        { marshmallows: 2 },
      ],
    });
  });
});

describe('handlers', () => {
  beforeEach(() => {
    client.addMethod('sort', ['array'], async (data: number[]) => {
      if (!Array.isArray(data)) {
        throw new RpcError(ErrorCodes.InvalidParams, 'Param is not an array');
      }

      return data.sort((a, b) => a - b);
    });

    client.addMethod('failing', [], async () => {
      throw new Error('Handler broken');
    });
  });

  test('calls the handler and returns the result', async () => {
    ioMock.mockIncoming({
      jsonrpc: '2.0',
      id: 5,
      method: 'sort',
      params: [
        [3, 1, 2]
      ],
    });

    const result = await ioMock.waitOutgoing();
    expect(result).toEqual({
      jsonrpc: '2.0',
      id: 5,
      result: [1, 2, 3],
    });
  });

  test('handles handler errors', async () => {
    ioMock.mockIncoming({
      jsonrpc: '2.0',
      id: 5,
      method: 'sort',
      params: [
        'invalid',
      ],
    });

    const result = await ioMock.waitOutgoing();
    expect(result).toEqual({
      jsonrpc: '2.0',
      id: 5,
      error: {
        code: ErrorCodes.InvalidParams,
        message: 'Param is not an array',
      },
    });
  });

  test('handles named parameters', async () => {
    ioMock.mockIncoming({
      jsonrpc: '2.0',
      id: 5,
      method: 'sort',
      params: { array: [2, 1] },
    });

    const result = await ioMock.waitOutgoing();
    expect(result).toMatchObject({
      id: 5,
      result: [1, 2],
    });
  });

  test('handles invalid parameters', async () => {
    ioMock.mockIncoming({
      jsonrpc: '2.0',
      id: 5,
      method: 'sort',
      params: { data: [2, 1] },
    });

    const result = await ioMock.waitOutgoing();
    expect(result).toMatchObject({
      id: 5,
      error: {
        code: ErrorCodes.InvalidParams,
      }
    });
  });

  test('handles invalid parameters 2', async () => {
    ioMock.mockIncomingInvalid(JSON.stringify({
      jsonrpc: '2.0',
      id: 5,
      method: 'sort',
      params: 'data',
    }));

    const result = await ioMock.waitOutgoing();
    expect(result).toMatchObject({
      id: 5,
      error: {
        code: ErrorCodes.InvalidParams,
      }
    });
  });

  test('handles invalid parameter count', async () => {
    ioMock.mockIncoming({
      jsonrpc: '2.0',
      id: 5,
      method: 'sort',
      params: [],
    });

    const result = await ioMock.waitOutgoing();
    expect(result).toMatchObject({
      id: 5,
      error: {
        code: ErrorCodes.InvalidParams,
      }
    });
  });

  test('handles invalid method', async () => {
    ioMock.mockIncoming({
      jsonrpc: '2.0',
      id: 5,
      method: 'sorrt',
      params: [],
    });

    const result = await ioMock.waitOutgoing();
    expect(result).toMatchObject({
      id: 5,
      error: {
        code: ErrorCodes.MethodNotFound,
      }
    });
  });

  test('invokes handler error when handler fails with error', async () => {
    const errorPromise = client.handlerErrors.pipe(take(1)).toPromise();
    ioMock.mockIncoming({
      jsonrpc: '2.0',
      id: 5,
      method: 'failing',
      params: null,
    });

    const error = await errorPromise;
    expect(error!.message).toBe('Handler broken');
  });

  test('does not return response for notification', async () => {
    ioMock.mockIncoming({
      jsonrpc: '2.0',
      method: 'sort',
      params: [[]],
    });
    ioMock.mockIncoming({
      jsonrpc: '2.0',
      id: 5,
      method: 'sort',
      params: [[]],
    });

    const result = await ioMock.waitOutgoing();
    expect(result).toMatchObject({ id: 5 });
  });

  test('does not allow for method to be added twice', async () => {
    expect(() => client.addMethod('sort', [], async () => null)).toThrowError('Method sort is already registered');
  });
});

describe('end', () => {
  test('ends all pending requests', async () => {
    const request = client.call('solve', 'y=x*3', { x: 3 });
    expect(ioMock.getOutgoing()).toBeTruthy();

    client.end();

    await expect(request).rejects.toBeInstanceOf(RpcError);
  });

  test('emits end', async () => {
    const endPromise = client.ended.toPromise();
    client.end();
    await endPromise;
  });

  test('does not allow for more requests', async () => {
    client.end();

    const request = client.notify('hello');
    await expect(request).rejects.toBeInstanceOf(RpcError);
    await expect(request).rejects.toMatchObject({ message: 'The client was ended.' });
  });

  test('does not send reply after end', async () => {
    const deferred = defer();
    client.addMethod('hello', [], () => deferred.promise);

    ioMock.mockIncoming({
      jsonrpc: '2.0',
      id: 3,
      method: 'hello',
      params: [],
    });

    client.end();

    deferred.resolve(null);
    await waitTick();

    expect(ioMock.getOutgoing()).toBe(null);
  });
});

describe('errors', () => {
  test('reports errors send from the other side', async () => {
    const errorPromise = client.errors.pipe(take(1)).toPromise();
    ioMock.mockIncoming({
      jsonrpc: '2.0',
      error: {
        code: ErrorCodes.InternalError,
        message: 'Something is wrong',
      }
    });
    const error = await errorPromise;
    expect(error!.message).toMatch(/^Generic error/);
    errors.pop();
  });

  test('throws RpcError when sending fails', async () => {
    ioMock.pushLine = jest.fn(() => Promise.reject(new Error('Cannot send line')));

    const request = client.call('hello');

    await expect(request).rejects.toBeInstanceOf(RpcError);
    expect(await request.catch(e => e.message)).toBe('Cannot send request: Cannot send line');
  });

  test('handles invalid requests', async () => {
    ioMock.mockIncomingInvalid(JSON.stringify({ }));

    let result = await ioMock.waitOutgoing();
    expect(result).toMatchObject({
      error: {
        code: ErrorCodes.InvalidRequest,
      }
    });

    ioMock.mockIncoming({ jsonrpc: '2.0' });

    result = await ioMock.waitOutgoing();
    expect(result).toMatchObject({
      error: {
        code: ErrorCodes.InvalidRequest,
      }
    });

    ioMock.mockIncomingInvalid('bla');

    result = await ioMock.waitOutgoing();
    expect(result).toMatchObject({
      error: {
        code: ErrorCodes.ParseError,
      }
    });
  });
  test('handles invalid error', async () => {
    const request = client.call('solve', 'y=x*3', { x: 3 });

    expect(ioMock.getOutgoing()).toMatchObject({
      id: 1,
    });

    ioMock.mockIncoming({ jsonrpc: '2.0', id: 1, error: { code: 1, message: undefined! } });

    const result = await ioMock.waitOutgoing();
    expect(result).toMatchObject({
      error: {
        code: ErrorCodes.InternalError,
      }
    });

    await expect(request).rejects.toBeInstanceOf(RpcError);
  });
});

describe('timeouts', () => {
  test('call throws timeout error if it misses the deadline', async () => {
    const request = client.call('hello');
    ioMock.getOutgoing();

    currentTimestamp += DEFAULT_TIMEOUT;
    jest.advanceTimersByTime(1000);

    await expect(request).rejects.toBeInstanceOf(RpcError);
    await expect(request).rejects.toMatchObject({ message: 'Request timeout' });

    expect(errors).toHaveLength(1);
    errors.pop();
  });
});

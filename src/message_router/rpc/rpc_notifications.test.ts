import { Subject } from 'rxjs';

import { RpcNotifications } from './rpc_notifications';
import { RpcClient, MethodHandler } from './rpc_client';

class RpcClientMock {
  methods: {
    method: string;
    params: string[];
    handler: MethodHandler;
  }[] = [];

  ended = new Subject<true>();

  addMethod(method: string, params: string[], handler: MethodHandler): void {
    this.methods.push({ method, params, handler });
  }
}

let clientMock: RpcClientMock;
let notifications: RpcNotifications;

beforeEach(() => {
  clientMock = new RpcClientMock();
  notifications = new RpcNotifications(clientMock as unknown as RpcClient);
});

afterEach(() => {
  notifications = null!;
  clientMock = null!;
});

test('delivers notifications from client', async () => {
  const pings = notifications.getNotifications<[number]>('ping', ['ts']);
  const timestamps: number[] = [];
  pings.subscribe(([ts]) => timestamps.push(ts));

  expect(clientMock.methods[0]).toMatchObject({ method: 'ping', params: ['ts'] });

  await clientMock.methods[0].handler(12);
  await clientMock.methods[0].handler(9);
  await clientMock.methods[0].handler(13);

  expect(timestamps).toMatchObject([12, 9, 13]);
});

test('completes subscriptions on client end', () => {
  const pings = notifications.getNotifications<[number]>('ping', []);
  let completed = false;
  pings.subscribe({
    complete: () => completed = true,
  });

  clientMock.ended.next(true);
  clientMock.ended.complete();

  expect(completed).toBe(true);
});

test('handles multiple subscriptions', async () => {
  const pings1 = notifications.getNotifications<[number]>('ping', ['ts']);

  const timestamps1: number[] = [];
  pings1.subscribe(([ts]) => timestamps1.push(ts));

  await clientMock.methods[0].handler(12);

  const pings2 = notifications.getNotifications<[number]>('ping', ['ts']);

  const timestamps2: number[] = [];
  pings2.subscribe(([ts]) => timestamps2.push(ts));

  await clientMock.methods[0].handler(9);
  await clientMock.methods[0].handler(10);

  expect(timestamps1).toMatchObject([12, 9, 10]);
  expect(timestamps2).toMatchObject([9, 10]);
});

test('throws error on inconsistent subscriptions', () => {
  notifications.getNotifications('ping', ['ts']);
  expect(() =>  notifications.getNotifications('ping', ['timestamp'])).toThrowError('Args mismatch: timestamp !== ts');
});

test('throws error on subscribing after client ends', () => {
  clientMock.ended.next(true);
  clientMock.ended.complete();

  expect(() =>  notifications.getNotifications('ping', ['ts'])).toThrowError('Underlying client has ended');
});

describe('hasSubscriptions', () => {
  test('returns true when there are subscribers', () => {
    const pings = notifications.getNotifications<[number]>('ping', ['ts']);
    expect(notifications.hasSubscriptions()).toBe(false);
    const subscription = pings.subscribe();
    expect(notifications.hasSubscriptions()).toBe(true);
    subscription.unsubscribe();
    expect(notifications.hasSubscriptions()).toBe(false);
  });
});

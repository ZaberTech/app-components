import { Subject, Observable, ReplaySubject } from 'rxjs';
import type { AnyAsyncFunction } from '@zaber/toolbox/lib/types';
import { ensureError, makeString } from '@zaber/toolbox';

import { RpcError } from './rpc_error';
import { ErrorCodes, Request, Response } from './types';

export interface RpcClientIO {
  pushLine(line: string): Promise<void>;
  setLineCallback(callback: (line: string) => void): void;
  isReliable: boolean;
}

export interface CallOptions {
  timeout?: number;
}

const defaultCallOptions: Required<CallOptions> = {
  timeout: 10 * 1000,
};

const MIN_ID = 1;
const MAX_ID = 100000;

function isID(id: unknown): boolean {
  return ['number', 'string'].includes(typeof id);
}
function isValidError(error: Response['error']) {
  return typeof error === 'object' && typeof error.code === 'number' && typeof error.message === 'string';
}

export type MethodHandler = AnyAsyncFunction;

interface MethodInfo {
  handler: MethodHandler;
  params: string[];
}

interface TimeMeasuring {
  now(): number;
}

export class RpcClient {
  private id: number = 1;
  private replies = new Map<number, {
    resolve: (result: unknown) => void;
    reject: (err: Error) => void;
    deadline: number;
  }>();

  private methods = new Map<string, MethodInfo>();

  private _ended = false;
  private endedSubject = new ReplaySubject<true>();
  public get ended(): Observable<true> {
    return this.endedSubject;
  }

  private errorsSubject = new Subject<Error>();
  public get errors(): Observable<Error> {
    return this.errorsSubject;
  }

  private handlerErrorsSubject = new Subject<Error>();
  public get handlerErrors(): Observable<Error> {
    return this.handlerErrorsSubject;
  }

  private options: Required<CallOptions>;

  private timeoutCheck: ReturnType<typeof setInterval> | null = null;

  constructor(
    private readonly io: RpcClientIO,
    private readonly time: TimeMeasuring,
    defaultOptions: CallOptions = {},
  ) {
    this.options = {
      ...defaultCallOptions,
      ...defaultOptions,
    };

    this.io.setLineCallback(this.onLine.bind(this));

    if (!this.io.isReliable) {
      this.timeoutCheck = setInterval(this.checkTimeouts.bind(this), 1000);
    }

    this.onError = this.onError.bind(this);
    this.onHandlerError = this.onHandlerError.bind(this);
  }

  private onError = (err: Error): void => {
    this.errorsSubject.next(err);
  };

  private onHandlerError = (err: Error): void => {
    this.handlerErrorsSubject.next(err);
  };

  private getId(): number {
    const id = this.id++;
    if (this.id >= MAX_ID) {
      this.id = MIN_ID;
    }
    return id;
  }

  private onLine(line: string): void {
    try {
      let incoming: Response | Request;
      try {
        incoming = JSON.parse(line);
      } catch {
        this.sendError(ErrorCodes.ParseError, `Cannot parse JSON: ${line}`);
        return;
      }

      if (incoming.jsonrpc !== '2.0') {
        this.sendError(ErrorCodes.InvalidRequest, `Not supported: ${line}`);
        return;
      }

      const isRequest = typeof (incoming as Request).method === 'string';
      if (isRequest) {
        return this.onRequest(incoming as Request);
      }

      const isResponse = isID((incoming as Response).id);
      if (isResponse) {
        return this.onResponse(incoming as Response);
      }

      const isGenericError = !!(incoming as Response).error;
      if (isGenericError) {
        return this.onError(new Error(`Generic error: ${line}`));
      }

      this.sendError(ErrorCodes.InvalidRequest, `Do not understand: ${line}`);
    } catch (err) {
      this.onHandlerError(ensureError(err));
    }
  }

  private onRequest(request: Request): void {
    const methodInfo = this.methods.get(request.method);
    if (!methodInfo) {
      this.sendError(ErrorCodes.MethodNotFound, `Method ${request.method} not found`, request.id);
      return;
    }

    let callParams: unknown[];
    if (typeof request.params === 'undefined' || request.params === null) {
      callParams = [];
    } else if (Array.isArray(request.params)) {
      if (request.params.length !== methodInfo.params.length) {
        return this.sendError(
          ErrorCodes.InvalidParams,
          `Params count does not match ${request.params.length} !== ${methodInfo.params.length}`,
          request.id
        );
      }

      callParams = request.params;
    } else if (typeof request.params === 'object') {
      const paramKeys = Object.keys(request.params);

      const hasAllParams = paramKeys.length === methodInfo.params.length
        && methodInfo.params.every(paramName => paramKeys.includes(paramName));

      if (!hasAllParams) {
        return this.sendError(
          ErrorCodes.InvalidParams,
          `Params names do not match [${paramKeys.join(',')}] !~~ [${methodInfo.params.join(',')}]`,
          request.id
        );
      }

      callParams = methodInfo.params.map(paramName => (request.params as Record<string, unknown>)[paramName]);
    } else {
      this.sendError(ErrorCodes.InvalidParams, `Invalid params ${makeString(request.params)}`, request.id);
      return;
    }

    this.callHandler(methodInfo, callParams, request).catch(this.onHandlerError);
  }

  private async callHandler(methodInfo: MethodInfo, params: unknown[], request: Request): Promise<void> {
    try {
      const result = await methodInfo.handler(...params);

      const isNotification = !isID(request.id);
      if (isNotification) {
        return;
      }

      if (this._ended) {
        return;
      }

      const response = JSON.stringify({
        jsonrpc: '2.0',
        id: request.id,
        result,
      });
      await this.pushLine(response);
    } catch (err) {
      if (err instanceof RpcError) {
        return this.sendError(err.code, err.message, request.id, err.data);
      }

      throw err;
    }
  }

  private onResponse(response: Response): void {
    const handler = this.replies.get(response.id as number);
    if (!handler) {
      return;
    }

    this.replies.delete(response.id as number);

    if (response.error) {
      let error: RpcError;
      if (isValidError(response.error)) {
        const { code, data, message } = response.error;
        error = new RpcError(code, message, data);
      } else {
        this.sendError(ErrorCodes.InternalError, `The error is invalid: ${JSON.stringify(response.error)}`);
        error = new RpcError(-1, 'Invalid error send by the other side');
      }
      handler.reject(error);
    } else {
      handler.resolve(response.result);
    }
  }

  public call<Return>(method: string, ...params: unknown[]): Promise<Return> {
    return this.callOpt(this.options, method, ...params);
  }
  public async callOpt<Return>(options: CallOptions, method: string, ...params: unknown[]): Promise<Return> {
    this.checkEnded();

    const id = this.getId();
    const request = JSON.stringify({
      jsonrpc: '2.0',
      id,
      method,
      params,
    });
    const promise = new Promise((resolve, reject) => {
      this.replies.set(id, {
        resolve,
        reject,
        deadline: this.time.now() + (options.timeout ?? this.options.timeout),
      });
    });
    const [result] = await Promise.all([
      promise,
      this.pushLine(request),
    ]);
    return result as Return;
  }

  public async notify(method: string, ...params: unknown[]): Promise<void> {
    this.checkEnded();

    const request = JSON.stringify({
      jsonrpc: '2.0',
      method,
      params,
    });
    await this.pushLine(request);
  }

  public end(): void {
    if (this._ended) {
      return;
    }
    this._ended = true;

    if (this.timeoutCheck !== null) {
      clearInterval(this.timeoutCheck);
      this.timeoutCheck = null;
    }

    const handlers = Array.from(this.replies.values());
    this.replies.clear();

    for (const { reject } of handlers) {
      reject(new RpcError(-1, 'The client was ended.'));
    }

    this.errorsSubject.complete();
    this.handlerErrorsSubject.complete();

    this.endedSubject.next(true);
    this.endedSubject.complete();
  }

  private checkEnded(): void {
    if (this._ended) {
      throw new RpcError(-1, 'The client was ended.');
    }
  }

  private sendError(code: number, message: string, id?: number | string, data?: unknown): void {
    if (this._ended) {
      return;
    }

    const response: Response = {
      jsonrpc: '2.0',
      error: {
        code,
        message,
        data,
      },
    };

    if (isID(id)) {
      response.id = id;
    }

    this.pushLine(JSON.stringify(response)).catch(this.onError);
  }

  public addMethod(method: string, params: string[], handler: MethodHandler): void {
    if (this.methods.has(method)) {
      throw new Error(`Method ${method} is already registered`);
    }

    this.methods.set(method, {
      handler,
      params: params || [],
    });
  }

  private async pushLine(line: string): Promise<void> {
    try {
      await this.io.pushLine(line);
    } catch (err) {
      throw new RpcError(-1, `Cannot send request: ${ensureError(err).message}`);
    }
  }

  private checkTimeouts(): void {
    const now = this.time.now();
    for (const [id, { deadline, reject }] of this.replies.entries()) {
      if (now < deadline) {
        continue;
      }

      this.replies.delete(id);

      const error = new RpcError(-1, 'Request timeout');
      reject(error);
      this.onError(error);
    }
  }
}

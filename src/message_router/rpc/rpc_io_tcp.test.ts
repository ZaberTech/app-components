/* eslint-disable @typescript-eslint/no-this-alias */
import { EventEmitter } from 'events';

import { take } from 'rxjs/operators';

let socketMock: SocketMock;

class SocketMock extends EventEmitter {
  public readonly lines: string[] = [];
  constructor() {
    super();
    socketMock = this;
  }

  pipe = jest.fn(() => ({ on: this.pipeOn }));
  pipeOn = jest.fn();
  end = jest.fn((callback: () => void) => { setImmediate(callback) });
  write = jest.fn((line: string, callback: () => void) => {
    setImmediate(callback);
    this.lines.push(line);
  });
}

jest.mock('net', () => ({
  createConnection: () => new SocketMock(),
}));

import { waitTick } from '../../test';

import { RpcIoTcp } from './rpc_io_tcp';

let io: RpcIoTcp;
let linesOut: string[];

beforeEach(() => {
  io = new RpcIoTcp({
    path: 'path',
  });
  linesOut = [];
  io.setLineCallback(line => linesOut.push(line));
});

afterEach(() => {
  socketMock = null!;
  io = null!;
  linesOut = null!;
});

test('connects on emit of connect event', async () => {
  const connectPromise = io.connect();

  socketMock.emit('connect');

  await connectPromise;
});

test('throws error on emit of error event', async () => {
  const connectPromise = io.connect();

  socketMock.emit('error', new Error('Cannot connect'));

  await expect(connectPromise).rejects.toMatchObject({ message: 'Cannot connect' });
});

describe('connected', () => {
  beforeEach(async () => {
    const connectPromise = io.connect();
    socketMock.emit('connect');
    await connectPromise;
  });

  test('emits the lines from the socket', async () => {
    expect(socketMock.pipeOn).toBeCalledTimes(1);
    const callback = socketMock.pipeOn.mock.calls[0][1];

    callback('data1');
    callback('data2');

    expect(linesOut).toEqual(['data1', 'data2']);
  });

  test('writes the lines to the socket', async () => {
    await io.pushLine('data1');
    await io.pushLine('data2');

    expect(socketMock.lines).toEqual(['data1', 'data2']);
  });

  test('updates last message timestamp', async () => {
    await io.pushLine('data1');

    expect(io.lastMessageTs > 0).toBe(true);
  });

  describe('close', () => {
    test('ends the socket', async () => {
      await io.close();
      expect(socketMock.end).toBeCalledTimes(1);
    });

    test('multiples closes all wait for the first end to finish', async () => {
      let callback: () => void;
      socketMock.end.mockImplementationOnce(cb => callback = cb);

      let resolved = false;
      const close1Promise = io.close().then(() => resolved = true);
      const close2Promise = io.close().then(() => resolved = true);

      expect(socketMock.end).toBeCalledTimes(1);

      await waitTick();
      expect(resolved).toBe(false);

      callback!();

      await Promise.all([close1Promise, close2Promise]);
    });
  });

  test('emits error when the sockets errors', async () => {
    const errorPromise = io.errors.pipe(take(1)).toPromise();

    socketMock.emit('error', new Error('socket failed'));

    expect(await errorPromise).toMatchObject({ message: 'socket failed' });
  });

  test('emits error when the sockets closes unexpectedly', async () => {
    const errorPromise = io.errors.pipe(take(1)).toPromise();

    socketMock.emit('close');

    expect(await errorPromise).toMatchObject({ message: 'Connection was closed by the other side.' });
  });
});

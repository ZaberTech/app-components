export class RpcError extends Error {
  constructor(
    public readonly code: number,
    message: string,
    public readonly data?: unknown,
  ) {
    super(message);
    Object.setPrototypeOf(this, RpcError.prototype);
  }
}

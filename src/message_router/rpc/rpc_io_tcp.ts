import { createConnection, NetConnectOpts, Socket } from 'net';

import split2 from 'split2';
import { ReplaySubject, Observable } from 'rxjs';

type OnLineCallback = (line: string) => void;

export class RpcIoTcp {
  private onLineCallback?: OnLineCallback;
  private socket: Socket | null = null;

  private closing?: Promise<void>;
  private closed = false;

  public readonly isReliable = true;

  private errorSubject = new ReplaySubject<Error>();
  public get errors(): Observable<Error> {
    return this.errorSubject;
  }

  private _lastMessageTs: number = 0;
  public get lastMessageTs(): number { return this._lastMessageTs }

  constructor(private readonly options: NetConnectOpts) {
  }

  public async connect(): Promise<void> {
    if (!this.onLineCallback) {
      throw new Error('onLine callback is not registered');
    }
    if (this.socket) {
      throw new Error('Connect already called');
    }

    this.socket = createConnection(this.options);

    this.socket.once('error', err =>
      this.errorSubject.next(err)
    );
    this.socket.once('close', () => {
      if (this.socket) {
        this.errorSubject.next(new Error('Connection was closed by the other side.'));
        this.socket = null;
      }
      this.errorSubject.complete();
    });

    this.socket.pipe(split2('\n')).on('data', (line: string) => this.onLineCallback?.(line));

    await new Promise((resolve, reject) => {
      this.socket!.once('connect', resolve);
      this.socket!.once('error', reject);
    });
  }

  public setLineCallback(callback: OnLineCallback): void {
    this.onLineCallback = callback;
  }

  public async pushLine(line: string): Promise<void> {
    if (!this.socket) {
      throw new Error('Connection is closed');
    }

    this._lastMessageTs = performance.now();
    await new Promise<void>((resolve, reject) =>
      this.socket!.write(line, err => err ? reject(err) : resolve())
    );
  }

  public async close(): Promise<void> {
    if (this.closed) {
      return;
    }

    if (this.closing) {
      return await this.closing;
    }

    if (this.socket) {
      this.closing = new Promise(resolve => this.socket!.end(resolve));
      await this.closing;
    }

    this.closed = true;
  }
}

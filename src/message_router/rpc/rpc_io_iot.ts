import { ensureError } from '@zaber/toolbox';
import { ReplaySubject, Observable, Subscription } from 'rxjs';

import { IotConnection, IoTService } from '../../iot';

type OnLineCallback = (line: string) => void;

export class RpcIoIoT {
  private onLineCallback?: OnLineCallback;
  private closed = false;
  private messages?: Subscription;
  private connection?: IotConnection;

  public readonly isReliable = false;

  private errorSubject = new ReplaySubject<Error>();
  public get errors(): Observable<Error> {
    return this.errorSubject;
  }

  private _lastMessageTs: number = 0;
  public get lastMessageTs(): number { return this._lastMessageTs }

  constructor(private readonly iotService: IoTService, private readonly options: { routerId: string; realm: string }) {
  }

  public async connect(): Promise<void> {
    if (!this.onLineCallback) {
      throw new Error('onLine callback is not registered');
    }
    if (this.connection) {
      throw new Error('Connect already called');
    }

    try {
      this.connection = await this.iotService.getConnection();

      const rpcRxTopic = `${this.options.realm}/message-router/${this.connection.clientId}/${this.options.routerId}/rpc`;

      const observable = await this.connection.subscribe(rpcRxTopic);
      this.messages = observable.subscribe(
        message => this.onLineCallback!(message.payload.toString('utf8')),
        error => this.errorSubject.next(error as Error),
      );
    } catch (thrown) {
      const err = ensureError(thrown);
      this.errorSubject.next(err);
      throw err;
    }
  }

  public setLineCallback(callback: OnLineCallback): void {
    this.onLineCallback = callback;
  }

  public async pushLine(line: string): Promise<void> {
    this._lastMessageTs = performance.now();

    const rpcTxTopic = `${this.options.realm}/message-router/${this.options.routerId}/${this.connection!.clientId}/rpc`;
    await this.connection!.publish(rpcTxTopic, line);
  }

  public async close(): Promise<void> {
    if (this.closed) {
      return;
    }
    this.closed = true;

    if (this.messages) {
      this.messages.unsubscribe();
    }
    this.errorSubject.complete();
  }
}

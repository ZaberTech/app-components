export interface Request {
  jsonrpc: string;
  id?: number|string;
  method: string;
  params: unknown[] | Record<string, unknown> | null;
}

export interface Response {
  jsonrpc: string;
  id?: number|string;
  result?: unknown;
  error?: {
    code: number;
    message: string;
    data?: unknown;
  };
}

export enum ErrorCodes {
  ParseError = -32700, // Invalid JSON was received by the server.
  InvalidRequest = -32600, // The JSON sent is not a valid Request object.
  MethodNotFound = -32601, // The method does not exist / is not available.
  InvalidParams = -32602,  // Invalid method parameter(s).
  InternalError = -32603, // Internal JSON-RPC error.
}

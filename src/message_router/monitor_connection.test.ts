/* eslint-disable @typescript-eslint/no-this-alias */

import { from, Observable, Subscriber } from 'rxjs';
import { mergeMap, share, take, toArray } from 'rxjs/operators';

import { RpcClient, RpcNotifications } from './rpc';
import { MonitorConnection, ReceivedMessage } from './monitor_connection';

let connection: MonitorConnection;
let notificationMock: NotificationMock;

class NotificationMock {
  public subscribers = new Map<string, Subscriber<unknown[]>>();
  constructor() {
    notificationMock = this;
  }

  getNotifications = jest.fn((method: string) => new Observable<unknown[]>(subscriber => {
    this.subscribers.set(method, subscriber);
    return () => { this.subscribers.delete(method) };
  }).pipe(share()));

  monitorMessages(messages: ReceivedMessage[], id: number): void {
    this.subscribers.get('routing_monitorMessage')!.next(['connection_id', messages, id]);
  }
}

let rpcMock: RpcMock;

class RpcMock {
  constructor() {
    rpcMock = this;
  }

  call = jest.fn(() => Promise.resolve());
}


beforeEach(() => {
  connection = new MonitorConnection(
    'connection_id',
    (new RpcMock() as unknown) as RpcClient,
    (new NotificationMock() as unknown) as RpcNotifications,
  );
});

afterEach(async () => {
  await connection.stop();
  connection = null!;

  notificationMock = null!;
  rpcMock = null!;
});

test('monitors messages', async () => {
  const promise = connection.monitorMessages.pipe(mergeMap(messages => from(messages)), take(4), toArray()).toPromise();

  await connection.start();
  expect(rpcMock.call).toHaveBeenLastCalledWith('routing_startMonitoring', 'connection_id');

  notificationMock.monitorMessages([{ l: '1', s: 'reply' }], 1);
  notificationMock.monitorMessages([{ l: '0', s: 'cmd' }], 0);
  notificationMock.monitorMessages([{ l: '3', s: 'broadcast' }], 3);
  notificationMock.monitorMessages([{ l: '2', s: 'cmd' }], 2);

  const messages = await promise;
  expect(messages).toEqual([{
    source: 'cmd',
    message: '0',
  }, {
    source: 'reply',
    message: '1',
  }, {
    source: 'cmd',
    message: '2',
  }, {
    source: 'broadcast',
    message: '3',
  }]);

  await connection.stop();
  expect(rpcMock.call).toHaveBeenLastCalledWith('routing_stopMonitoring', 'connection_id');
});

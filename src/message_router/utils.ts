interface Version { major: number; minor: number }

export function parseApiVersion(version: string): Version | null {
  if (typeof version !== 'string' || !version.match(/^\d+\.\d+$/)) {
    return null;
  }
  const [major, minor] = version.split('.').map(Number);
  return { major, minor };
}

/** v1 >= v2 */
export function versionGte(v1: Version, v2: Version) {
  return v1.major > v2.major || (v1.major === v2.major && v1.minor >= v2.minor);
}

/** version ∈ ^required */
export function versionCompatible(version: Version, required: Version) {
  return version.major === required.major && version.minor >= required.minor;
}

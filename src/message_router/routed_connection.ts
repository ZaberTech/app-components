import { ascii, ConnectionFailedException } from '@zaber/motion';
import { ensureArray, ensureError } from '@zaber/toolbox';
import { Observable, ReplaySubject } from 'rxjs';
import { filter, map, takeUntil } from 'rxjs/operators';
import { eachValueFrom } from 'rxjs-for-await';

import { RpcClient } from './rpc/rpc_client';
import { RpcError } from './rpc/rpc_error';
import { RpcNotifications } from './rpc/rpc_notifications';
import { ensureMessageOrder } from './message_ordering';
import { RoutedConnectionOptions } from './types';

export class RoutedConnection {
  private _connection?: ascii.Connection;
  private transport?: ascii.Transport;

  private opened = false;
  private opening?: Promise<void>;

  private readLoopPromise?: Promise<void>;
  private writeLoopPromise?: Promise<void>;

  public get connection(): ascii.Connection {
    if (!this._connection) { throw new Error('Connection not opened') }
    return this._connection;
  }

  private endSubscriptions = new ReplaySubject();

  private endedSubject = new ReplaySubject<true>();
  public readonly ended = this.endedSubject.asObservable();

  private nextMessageID = new Uint32Array([0]);

  constructor(
    public readonly id: string,
    private readonly rpc: RpcClient,
    private readonly notifications: RpcNotifications,
    private readonly options: RoutedConnectionOptions = {},
  ) {
    this.closeWithError = this.closeWithError.bind(this);
  }

  async ensureOpen(): Promise<void> {
    if (this.opened) {
      return;
    }
    if (this.opening) {
      return await this.opening;
    }

    this.opening = this.open();

    try {
      await this.opening;
    } catch (err) {
      this.onEnd();
      throw err;
    }
    this.opened = true;
  }

  async close(): Promise<void> {
    if (this.transport) {
      await this.transport.close();
    }
    if (this.readLoopPromise) {
      await this.readLoopPromise;
    }
    if (this.writeLoopPromise) {
      await this.writeLoopPromise;
    }
  }

  private async open(): Promise<void> {
    try {
      await this.rpc.call('routing_warmup', this.id);
    } catch (err) {
      if (err instanceof RpcError) {
        throw new ConnectionFailedException(err.message);
      }
      throw err;
    }

    this.transport = ascii.Transport.open();
    this._connection = await ascii.Connection.openCustom(this.transport);
    if (this.options.defaultTimeout != null) {
      this._connection.defaultRequestTimeout = this.options.defaultTimeout;
    }

    const repliesObservable = this.notifications.getNotifications<[string, string[] | string, number]>(
      'routing_reply', ['connection_id', 'replies', 'id']
    ).pipe(
      takeUntil(this.endSubscriptions),
      filter(([connectionId]) => connectionId === this.id),
      map(([, replies, id]) => ({ replies: ensureArray(replies), id })),
      ensureMessageOrder(),
    );

    const errorsObservable = this.notifications.getNotifications<[string, string]>(
      'routing_error', ['connection_id', 'message']
    ).pipe(
      takeUntil(this.endSubscriptions),
      filter(([connectionId]) => connectionId === this.id),
      map(([, errorMessage]) => errorMessage),
    );
    errorsObservable.subscribe(this.closeWithError);

    this.rpc.ended.pipe(
      takeUntil(this.endSubscriptions),
    ).subscribe(() => this.closeWithError('RPC Client ended'));

    this.writeLoopPromise = this.writeLoop(repliesObservable);
    this.readLoopPromise = this.readLoop();
  }

  private async readLoop(): Promise<void> {
    try {
      while (true) {
        const request = await this.transport!.read();

        await this.sendMessage(request);
      }
    } catch (err) {
      if (err instanceof RpcError) {
        await this.transport!.closeWithError(err.message);
      } else {
        await this.transport!.close();
      }
    }

    this.endSubscriptions.next(undefined);
    this.endSubscriptions.complete();

    await this.coolDown();

    // end must be emitted after cooldown to avoid race condition with potential new connection
    this.onEnd();
  }

  private async writeLoop(repliesObservable: Observable<{ replies: string[] }>): Promise<void> {
    try {
      for await (const { replies } of eachValueFrom(repliesObservable)) {
        for (const reply of replies) {
          await this.transport!.write(reply);
        }
      }
    } catch (err) {
      await this.transport!.closeWithError(ensureError(err).message);
    }
  }

  private closeWithError = (error: string | Error): void => {
    this.transport!.closeWithError(String(error)).catch(() => null);
  };

  private onEnd(): void {
    this.endedSubject.next(true);
    this.endedSubject.complete();
  }

  private async coolDown() {
    try {
      await this.rpc.call('routing_cooldown', this.id);
    } catch (err) {
      if (!(err instanceof RpcError)) {
        throw err;
      }
    }
  }

  public async sendMessage(message: string): Promise<void> {
    const messageID = this.nextMessageID[0]++;
    await this.rpc.notify('routing_sendMessage', this.id, message, messageID);
  }
}

import { ConnectionType } from './types';

export declare namespace API {
  export interface Connection {
    id: string;
    type: ConnectionType;
    name: string;
    hostname?: string;
    tcpPort?: number;
    serialPort?: string;
    baudRate?: number;
  }
}

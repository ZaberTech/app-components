export class RouterApiError extends Error {
  constructor(
    message: string,
  ) {
    super(message);
    Object.setPrototypeOf(this, RouterApiError.prototype);
  }
}

import _ from 'lodash';
import { convertForApi, convertFromApi, ensureError } from '@zaber/toolbox/lib';

import { RpcError } from './rpc/rpc_error';
import { RpcClient } from './rpc/rpc_client';
import {
  CloudConfig, CloudStatus, Connection, RawTcpConfig, SerialPortConfig, LocalShareConfig, LocalShareStatus,
  MIN_REQUIRED_API_VERSION, CLIENT_VERSION,
} from './types';
import { RouterApiError } from './router_api_error';
import { API } from './api_types';
import { parseApiVersion, versionCompatible, versionGte } from './utils';

function mapConnection(connection: API.Connection): Connection {
  return {
    id: connection.id,
    type: connection.type,
    config: _.pick(connection as Required<API.Connection>, 'serialPort', 'baudRate', 'hostname', 'tcpPort', 'name'),
  };
}

const CHECK_VERSION_TIMEOUT = 10 * 1000;

async function translateError<T>(f: () => Promise<T>): Promise<T> {
  try {
    return await f();
  } catch (err) {
    if (err instanceof RpcError) {
      throw new RouterApiError(err.message);
    }
    throw err;
  }
}

export class MessageRouterApi {
  constructor(private readonly rpc: RpcClient) {}

  public getConnections = async (): Promise<Connection[]> => await translateError(async () => {
    const connections = await this.rpc.call<unknown[]>('routing_listConnections');
    return connections.map(connection => mapConnection(convertFromApi(connection)));
  });

  public addTcpIp = async (config: Partial<RawTcpConfig>): Promise<Connection> => await translateError(async () => {
    const connection = await this.rpc.call(
      'config_addTCPIP', convertForApi(config));
    return mapConnection(convertFromApi(connection));
  });

  public addSerialPort = async (config: Partial<SerialPortConfig>): Promise<Connection> => await translateError(async () => {
    const connection = await this.rpc.call(
      'config_addSerialPort', convertForApi(config));
    return mapConnection(convertFromApi(connection));
  });

  public removeConnection = async (connectionId: string): Promise<void> => await translateError(async () => {
    await this.rpc.call<void>(
      'config_removeConnection',
      connectionId);
  });

  public setCloudConfig = async (config: CloudConfig | null): Promise<void> => await translateError(async () => {
    await this.rpc.call('config_setCloudConfig', convertForApi(config));
  });

  public getCloudConfig = async (): Promise<CloudConfig | null> => await translateError(async () => {
    const cloudConfig = await this.rpc.call('config_getCloudConfig');
    return convertFromApi(cloudConfig);
  });

  public getCloudStatus = async (): Promise<CloudStatus> => await translateError(async () => {
    const status = await this.rpc.call('cloud_getStatus');
    return convertFromApi(status);
  });

  public setLocalShareConfig = async (config: LocalShareConfig | null): Promise<void> => await translateError(async () => {
    await this.rpc.call<void>('config_setLocalShareConfig', convertForApi(config));
  });

  public getLocalShareConfig = async (): Promise<LocalShareConfig | null> => await translateError(async () => {
    const localShareConfig = await this.rpc.call('config_getLocalShareConfig');
    return convertFromApi(localShareConfig);
  });

  public getLocalShareStatus = async (): Promise<LocalShareStatus> => await translateError(async () => {
    const localShareConfig = await this.rpc.call('localshare_getStatus');
    return convertFromApi(localShareConfig);
  });

  public setName = async (name: string): Promise<void> => await translateError(async () => {
    await this.rpc.call('config_setName', name);
  });

  public setConnectionName = async (connectionId: string, name: string): Promise<void> => await translateError(async () => {
    await this.rpc.call('config_setConnectionName', connectionId, name);
  });

  public getName = (): Promise<string> => translateError(() => this.rpc.call<string>('status_getName'));

  public ensureCompatibility = async (): Promise<void> => translateError(async () => {
    let apiVersionString: string;
    try {
      apiVersionString = await this.rpc.callOpt<string>({ timeout: CHECK_VERSION_TIMEOUT }, 'status_getApiVersion');
    } catch (err) {
      const message = ensureError(err).message;
      throw new Error(`Initial handshake has failed, message router not compatible: ${message}`);
    }
    const apiVersion = parseApiVersion(apiVersionString);
    const requiredVersion = parseApiVersion(MIN_REQUIRED_API_VERSION)!;
    if (apiVersion == null || !versionCompatible(apiVersion, requiredVersion)) {
      throw new Error(`Message Router API ${apiVersionString} is not compatible ${MIN_REQUIRED_API_VERSION}`);
    }

    if (versionGte(apiVersion, { major: 1, minor: 2 })) {
      await this.rpc.callOpt({ timeout: CHECK_VERSION_TIMEOUT }, 'client_setVersion', CLIENT_VERSION);
    }
  });

  public ping = async (timeout?: number): Promise<void> => await translateError(async () => {
    await this.rpc.callOpt({ timeout }, 'status_ping');
  });
}

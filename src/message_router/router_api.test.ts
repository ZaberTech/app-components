/* eslint-disable camelcase */
import { convertForApi } from '@zaber/toolbox/lib';

import { RpcClient, RpcError } from './rpc';
import { API } from './api_types';
import { MessageRouterApi } from './router_api';
import { RouterApiError } from './router_api_error';
import { CLIENT_VERSION, CloudConfig, CloudStatus, ConnectionType, MIN_REQUIRED_API_VERSION } from './types';
import { parseApiVersion } from './utils';

const TEST_API_CONNECTION_SERIAL = convertForApi<API.Connection>({
  id: 'COM1',
  type: ConnectionType.SERIAL_PORT,
  name: '',
  serialPort: 'COM1',
  baudRate: 115200,
});
const TEST_API_CONNECTION_TCP_IP = convertForApi<API.Connection>({
  id: 'localhost:11321',
  type: ConnectionType.TCP_IP,
  name: '',
  hostname: 'localhost',
  tcpPort: 11321,
});

let api: MessageRouterApi;
const rpcClient = {
  call: jest.fn(() => Promise.resolve<unknown>(null)),
  callOpt: jest.fn(() => Promise.resolve<unknown>(null)),
};

beforeEach(() => {
  rpcClient.call.mockReset();
  rpcClient.callOpt.mockReset();
  api = new MessageRouterApi(rpcClient as unknown as RpcClient);
});

afterAll(() => {
  api = null!;
});

describe('getConnections', () => {
  test('returns connections of the router', async () => {
    rpcClient.call.mockImplementationOnce(() => Promise.resolve([
      TEST_API_CONNECTION_SERIAL,
      TEST_API_CONNECTION_TCP_IP,
    ]));

    const connections = await api.getConnections();
    expect(connections).toEqual([{
      id: 'COM1',
      type: ConnectionType.SERIAL_PORT,
      config: {
        serialPort: 'COM1',
        name: '',
        baudRate: 115200,
      },
    }, {
      id: 'localhost:11321',
      type: ConnectionType.TCP_IP,
      config: {
        hostname: 'localhost',
        name: '',
        tcpPort: 11321,
      },
    }]);
  });
});

describe('addTcpIp', () => {
  test('sends the correct request returning connection', async () => {
    rpcClient.call.mockImplementationOnce(() => Promise.resolve(TEST_API_CONNECTION_TCP_IP));

    const connection = await api.addTcpIp({ hostname: 'localhost', tcpPort: 11321 });

    expect(rpcClient.call).toBeCalledWith('config_addTCPIP', {
      hostname: 'localhost',
      tcp_port: 11321,
    });
    expect(connection).toEqual({
      id: 'localhost:11321',
      type: ConnectionType.TCP_IP,
      config: {
        hostname: 'localhost',
        name: '',
        tcpPort: 11321,
      },
    });
  });
});

describe('addSerialPort', () => {
  test('sends the correct request returning connection', async () => {
    rpcClient.call.mockImplementationOnce(() => Promise.resolve(TEST_API_CONNECTION_SERIAL));

    const connection = await api.addSerialPort({ serialPort: 'COM1', baudRate: 115200 });

    expect(rpcClient.call).toBeCalledWith('config_addSerialPort', {
      serial_port: 'COM1',
      baud_rate: 115200,
    });
    expect(connection).toEqual({
      id: 'COM1',
      type: ConnectionType.SERIAL_PORT,
      config: {
        serialPort: 'COM1',
        name: '',
        baudRate: 115200,
      },
    });
  });
});

describe('removeConnection', () => {
  test('sends the correct request', async () => {
    rpcClient.call.mockImplementationOnce(() => Promise.resolve());

    await api.removeConnection('localhost:11321');

    expect(rpcClient.call).toBeCalledWith('config_removeConnection', 'localhost:11321');
  });
});

describe('errors', () => {
  test('throws RouterApiError on RpcError', async () => {
    rpcClient.call.mockImplementationOnce(() => Promise.reject(new RpcError(-1, 'Something failed')));

    const request = api.removeConnection('localhost:11321');
    await expect(request).rejects.toBeInstanceOf(RouterApiError);
    expect(await request.catch(e => e.message)).toBe('Something failed');
  });

  test('passed ordinary error', async () => {
    rpcClient.call.mockImplementationOnce(() => Promise.reject(new Error('Ups')));

    const request = api.removeConnection('localhost:11321');
    await expect(request).rejects.toBeInstanceOf(Error);
  });
});

const TEST_CLOUD_CONFIG: CloudConfig = { apiUrl: 'url', id: 'id', realm: 'realm', token: 'token' };

describe('setCloudConfig', () => {
  test('sends the correct request', async () => {
    rpcClient.call.mockImplementationOnce(() => Promise.resolve());

    await api.setCloudConfig(TEST_CLOUD_CONFIG);
    await api.setCloudConfig(null);

    expect(rpcClient.call).toHaveBeenNthCalledWith(1,
      'config_setCloudConfig',
      { api_url: 'url', id: 'id', realm: 'realm', token: 'token' }
    );
    expect(rpcClient.call).toHaveBeenNthCalledWith(2,
      'config_setCloudConfig',
      null
    );
  });
});

describe('getCloudConfig', () => {
  test('sends the correct request returning config', async () => {
    rpcClient.call.mockImplementationOnce(() => Promise.resolve(convertForApi(TEST_CLOUD_CONFIG)));

    const cloudConfig = await api.getCloudConfig();
    expect(cloudConfig).toEqual(TEST_CLOUD_CONFIG);
  });
});

const TEST_CLOUD_STATUS: CloudStatus = {
  isConnected: true,
};

describe('cloud_getStatus', () => {
  test('sends the correct request returning status', async () => {
    rpcClient.call.mockImplementationOnce(() => Promise.resolve(convertForApi(TEST_CLOUD_STATUS)));

    const cloudStatus = await api.getCloudConfig();
    expect(cloudStatus).toEqual(TEST_CLOUD_STATUS);
  });
});

describe('ensureCompatibility', () => {
  const requiredVersion = parseApiVersion(MIN_REQUIRED_API_VERSION)!;

  test('defined version is parsable', () => {
    expect(requiredVersion).toBeTruthy();
  });
  test('passes if the version is compatible', async () => {
    const cases = [
      `${requiredVersion.major}.${requiredVersion.minor}`,
      `${requiredVersion.major}.${requiredVersion.minor + 1}`,
      `${requiredVersion.major}.1234567`,
    ];
    for (const version of cases) {
      rpcClient.callOpt.mockImplementationOnce(() => Promise.resolve(version));
      await api.ensureCompatibility();
    }
  });
  test('throw error if the version is not incompatible or invalid', async () => {
    const cases = [
      `${requiredVersion.major}.${requiredVersion.minor - 1}`,
      `${requiredVersion.major - 1}.${requiredVersion.minor}`,
      `${requiredVersion.major + 1}.${requiredVersion.minor}`,
      `${requiredVersion.major}.`,
      '.', '', '1.x', null, {},
    ];
    for (const version of cases) {
      rpcClient.callOpt.mockImplementationOnce(() => Promise.resolve(version));
      await expect(api.ensureCompatibility()).rejects.toMatchObject({
        message: expect.stringMatching(/is not compatible/)
      });
    }
  });
  test('throw error if it connects to something that may not be router', async () => {
    rpcClient.callOpt.mockRejectedValueOnce(new Error('Timeout'));
    await expect(api.ensureCompatibility()).rejects.toMatchObject({
      message: expect.stringMatching(/message router not compatible/)
    });
  });
  test('sends own version when router API is above 1.2', async () => {
    rpcClient.callOpt.mockImplementationOnce(() => Promise.resolve('1.1'));
    await api.ensureCompatibility();
    expect(rpcClient.callOpt).toHaveBeenCalledTimes(1);
    rpcClient.callOpt.mockClear();

    rpcClient.callOpt.mockImplementationOnce(() => Promise.resolve('1.2'));
    await api.ensureCompatibility();
    expect(rpcClient.callOpt).toHaveBeenCalledTimes(2);
    expect(rpcClient.callOpt.mock.calls[1]).toEqual([expect.anything(), 'client_setVersion', CLIENT_VERSION]);
  });
});

test('have not forgotten to bump the API version (or forgotten to update this test)', () => {
  // If you have added new methods perhaps you forgotten to bump the MIN_REQUIRED_API_VERSION.\
  const mra = new MessageRouterApi(rpcClient as unknown as RpcClient);
  expect(Object.keys(mra)).toEqual([
    'rpc',
    'getConnections',
    'addTcpIp',
    'addSerialPort',
    'removeConnection',
    'setCloudConfig',
    'getCloudConfig',
    'getCloudStatus',
    'setLocalShareConfig',
    'getLocalShareConfig',
    'getLocalShareStatus',
    'setName',
    'setConnectionName',
    'getName',
    'ensureCompatibility',
    'ping',
  ]);
  expect(MIN_REQUIRED_API_VERSION).toBe('1.0');
});

import { ReplaySubject, Subject } from 'rxjs';
import { filter, map, takeUntil } from 'rxjs/operators';

import { RpcClient } from './rpc/rpc_client';
import { RpcNotifications } from './rpc/rpc_notifications';
import { ensureMessageOrder } from './message_ordering';
import { MonitorMessage, MonitorMessageSource } from './types';

export interface ReceivedMessage {
  l: string; // line
  s: MonitorMessageSource; // source
}

export class MonitorConnection {
  private _monitorMessages = new Subject<MonitorMessage[]>();
  public readonly monitorMessages = this._monitorMessages.asObservable();

  private stopped: ReplaySubject<unknown> | null = null;

  constructor(
    public readonly id: string,
    private readonly rpc: RpcClient,
    private readonly notifications: RpcNotifications,
  ) { }

  public async start(): Promise<void> {
    if (this.stopped != null) {
      throw new Error('Start already called');
    }
    this.stopped = new ReplaySubject();

    this.notifications.getNotifications<[string, ReceivedMessage[], number]>(
      'routing_monitorMessage', ['connection', 'messages', 'id']
    ).pipe(
      takeUntil(this.stopped),
      filter(([connectionId]) => connectionId === this.id),
      map(([, messages, id]) => ({ messages, id })),
      ensureMessageOrder(),
      map(data => data.messages.map<MonitorMessage>(({ l, s }) => ({ message: l, source: s }))),
    ).subscribe(this._monitorMessages);

    try {
      await this.rpc.call('routing_startMonitoring', this.id);
    } catch (err) {
      this.stopSubscription();
      throw err;
    }
  }

  public async stop(): Promise<void> {
    if (this.stopped == null || this.stopped.isStopped) {
      return;
    }

    this.stopSubscription();
    await this.rpc.call('routing_stopMonitoring', this.id);
  }

  private stopSubscription() {
    if (this.stopped == null) { return }
    this.stopped.next(undefined);
    this.stopped.complete();
  }
}

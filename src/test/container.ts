import { Container } from 'inversify';

import { LocalStorageSymbol } from '../storage';
import { isTest } from '../utils';

let container: Container;

export function getContainer(): Container {
  return container;
}

export function createContainer(): Container {
  if (!isTest) {
    throw new Error('Container functions are only meant for testing. Create your own container in the app.');
  }

  container = new Container({ autoBindInjectable: true, defaultScope: 'Singleton' });

  if (global.localStorage) {
    container.bind(LocalStorageSymbol).toConstantValue(global.localStorage);
  }

  return container;
}

export function destroyContainer(): void {
  if (container) {
    container.unbindAll();
    container = null!;
  }
}

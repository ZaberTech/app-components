/* eslint-disable @typescript-eslint/no-explicit-any, @typescript-eslint/no-unsafe-member-access */
import 'reflect-metadata';
import '@zaber/toolbox/lib/test/setup';

/**
 * jsdom replaces Uint8Array for something not compatible which causes issues in libraries like protobuf.
 * This code reverts Uint8Array to the one provided by environment. */
(function fixUint8Array() {
  let findPrototype = Buffer as any;
  while (findPrototype.__proto__) {
    if (findPrototype.name === 'Uint8Array') {
      global.Uint8Array = findPrototype;
      break;
    }
    findPrototype = findPrototype.__proto__;
  }

  if (!(Buffer.from('test') instanceof Uint8Array)) {
    throw new Error('Buffer not compatible with Uint8Array');
  }
})();

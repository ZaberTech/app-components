import { LogLevel } from './types';

interface LogParent {
  log(level: LogLevel, name: string, ...args: unknown[]): void;
}

export class Logger {
  constructor(private readonly parent: LogParent, private readonly name: string) {
    this.info = this.info.bind(this);
    this.warn = this.warn.bind(this);
    this.error = this.error.bind(this);
  }

  public info(...args: unknown[]): void {
    this.parent.log(LogLevel.Info, this.name, ...args);
  }

  public warn(...args: unknown[]): void {
    this.parent.log(LogLevel.Warning, this.name, ...args);
  }

  public error(...args: unknown[]): void {
    this.parent.log(LogLevel.Error, this.name, ...args);
  }
}

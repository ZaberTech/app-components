import { injectable } from 'inversify';

import { isTest } from '../utils';

import { Logger } from './logger';
import { LogLevel } from './types';

@injectable()
export class Log {
  private readonly loggers = new Map<string, Logger>();
  private readonly logFunctions = {
    [LogLevel.Info]: console.log, // eslint-disable-line no-console
    [LogLevel.Warning]: console.warn, // eslint-disable-line no-console
    [LogLevel.Error]: console.error, // eslint-disable-line no-console
  };

  public getLogger(name: string): Logger {
    let logger = this.loggers.get(name);
    if (!logger) {
      logger = new Logger(this, name);
      this.loggers.set(name, logger);
    }
    return logger;
  }

  public log(level: LogLevel, name: string, ...args: unknown[]): void {
    if (isTest && level !== LogLevel.Error) {
      return;
    }

    const message = `${new Date().toISOString()} ${level} ${name}:`;
    this.logFunctions[level](message, ...args);
  }
}

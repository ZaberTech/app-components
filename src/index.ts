/*
  Don't export classes from folders here.
  Instead import them one by one in the app.
  It will allow webpack to shake off unused dependencies.
*/

export { Config } from './config';

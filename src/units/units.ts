// This file is generated from the Zaber device database. Do not manually edit this file.
/* eslint-disable */
// tslint:disable
import { DimensionDefinition } from './types';

export const dimensions: DimensionDefinition[] = [
  {
    "DimensionName": "Length",
    "LongName": "metres",
    "ShortName": "m",
    "units": [
      {
        "LongName": "metres",
        "Offset": 0,
        "Scale": 1,
        "ShortName": "m",
        "id": "Length:metres"
      },
      {
        "LongName": "centimetres",
        "Offset": 0,
        "Scale": 100,
        "ShortName": "cm",
        "id": "Length:centimetres"
      },
      {
        "LongName": "millimetres",
        "Offset": 0,
        "Scale": 1000,
        "ShortName": "mm",
        "id": "Length:millimetres"
      },
      {
        "LongName": "micrometres",
        "Offset": 0,
        "Scale": 1000000,
        "ShortName": "µm",
        "id": "Length:micrometres"
      },
      {
        "LongName": "nanometres",
        "Offset": 0,
        "Scale": 1000000000,
        "ShortName": "nm",
        "id": "Length:nanometres"
      },
      {
        "LongName": "inches",
        "Offset": 0,
        "Scale": 39.37007874015748,
        "ShortName": "in",
        "id": "Length:inches"
      }
    ],
    "defaultUnit": "Length:metres"
  },
  {
    "DimensionName": "Velocity",
    "LongName": "metres per second",
    "ShortName": "m/s",
    "units": [
      {
        "LongName": "metres per second",
        "Offset": 0,
        "Scale": 1,
        "ShortName": "m/s",
        "id": "Velocity:metres per second"
      },
      {
        "LongName": "centimetres per second",
        "Offset": 0,
        "Scale": 100,
        "ShortName": "cm/s",
        "id": "Velocity:centimetres per second"
      },
      {
        "LongName": "millimetres per second",
        "Offset": 0,
        "Scale": 1000,
        "ShortName": "mm/s",
        "id": "Velocity:millimetres per second"
      },
      {
        "LongName": "micrometres per second",
        "Offset": 0,
        "Scale": 1000000,
        "ShortName": "µm/s",
        "id": "Velocity:micrometres per second"
      },
      {
        "LongName": "nanometres per second",
        "Offset": 0,
        "Scale": 1000000000,
        "ShortName": "nm/s",
        "id": "Velocity:nanometres per second"
      },
      {
        "LongName": "inches per second",
        "Offset": 0,
        "Scale": 39.37007874015748,
        "ShortName": "in/s",
        "id": "Velocity:inches per second"
      }
    ],
    "defaultUnit": "Velocity:metres per second"
  },
  {
    "DimensionName": "Acceleration",
    "LongName": "metres per second squared",
    "ShortName": "m/s²",
    "units": [
      {
        "LongName": "metres per second squared",
        "Offset": 0,
        "Scale": 1,
        "ShortName": "m/s²",
        "id": "Acceleration:metres per second squared"
      },
      {
        "LongName": "centimetres per second squared",
        "Offset": 0,
        "Scale": 100,
        "ShortName": "cm/s²",
        "id": "Acceleration:centimetres per second squared"
      },
      {
        "LongName": "millimetres per second squared",
        "Offset": 0,
        "Scale": 1000,
        "ShortName": "mm/s²",
        "id": "Acceleration:millimetres per second squared"
      },
      {
        "LongName": "micrometres per second squared",
        "Offset": 0,
        "Scale": 1000000,
        "ShortName": "µm/s²",
        "id": "Acceleration:micrometres per second squared"
      },
      {
        "LongName": "nanometres per second squared",
        "Offset": 0,
        "Scale": 1000000000,
        "ShortName": "nm/s²",
        "id": "Acceleration:nanometres per second squared"
      },
      {
        "LongName": "inches per second squared",
        "Offset": 0,
        "Scale": 39.37007874015748,
        "ShortName": "in/s²",
        "id": "Acceleration:inches per second squared"
      }
    ],
    "defaultUnit": "Acceleration:metres per second squared"
  },
  {
    "DimensionName": "Angle",
    "LongName": "degrees",
    "ShortName": "°",
    "units": [
      {
        "LongName": "degrees",
        "Offset": 0,
        "Scale": 1,
        "ShortName": "°",
        "id": "Angle:degrees"
      },
      {
        "LongName": "radians",
        "Offset": 0,
        "Scale": 0.017453292519943295,
        "ShortName": "rad",
        "id": "Angle:radians"
      }
    ],
    "defaultUnit": "Angle:degrees"
  },
  {
    "DimensionName": "Angular Velocity",
    "LongName": "degrees per second",
    "ShortName": "°/s",
    "units": [
      {
        "LongName": "degrees per second",
        "Offset": 0,
        "Scale": 1,
        "ShortName": "°/s",
        "id": "Angular Velocity:degrees per second"
      },
      {
        "LongName": "radians per second",
        "Offset": 0,
        "Scale": 0.017453292519943295,
        "ShortName": "rad/s",
        "id": "Angular Velocity:radians per second"
      }
    ],
    "defaultUnit": "Angular Velocity:degrees per second"
  },
  {
    "DimensionName": "Angular Acceleration",
    "LongName": "degrees per second squared",
    "ShortName": "°/s²",
    "units": [
      {
        "LongName": "degrees per second squared",
        "Offset": 0,
        "Scale": 1,
        "ShortName": "°/s²",
        "id": "Angular Acceleration:degrees per second squared"
      },
      {
        "LongName": "radians per second squared",
        "Offset": 0,
        "Scale": 0.017453292519943295,
        "ShortName": "rad/s²",
        "id": "Angular Acceleration:radians per second squared"
      }
    ],
    "defaultUnit": "Angular Acceleration:degrees per second squared"
  },
  {
    "DimensionName": "AC Electric Current",
    "LongName": "amperes peak",
    "ShortName": "A pk",
    "units": [
      {
        "LongName": "amperes peak",
        "Offset": 0,
        "Scale": 1,
        "ShortName": "A(peak)",
        "id": "AC Electric Current:amperes peak"
      },
      {
        "LongName": "amperes RMS",
        "Offset": 0,
        "Scale": 0.7071067811865476,
        "ShortName": "A(RMS)",
        "id": "AC Electric Current:amperes RMS"
      }
    ],
    "defaultUnit": "AC Electric Current:amperes peak"
  },
  {
    "DimensionName": "Percent",
    "LongName": "percent",
    "ShortName": "%",
    "units": [
      {
        "LongName": "percent",
        "Offset": 0,
        "Scale": 1,
        "ShortName": "%",
        "id": "Percent:percent"
      }
    ],
    "defaultUnit": "Percent:percent"
  },
  {
    "DimensionName": "DC Electric Current",
    "LongName": "amperes peak",
    "ShortName": "A pk",
    "units": [
      {
        "LongName": "amperes",
        "Offset": 0,
        "Scale": 1,
        "ShortName": "A",
        "id": "DC Electric Current:amperes"
      }
    ],
    "defaultUnit": "DC Electric Current:amperes"
  },
  {
    "DimensionName": "Force",
    "LongName": "newtons",
    "ShortName": "N",
    "units": [
      {
        "LongName": "newtons",
        "Offset": 0,
        "Scale": 1,
        "ShortName": "N",
        "id": "Force:newtons"
      },
      {
        "LongName": "millinewtons",
        "Offset": 0,
        "Scale": 1000,
        "ShortName": "mN",
        "id": "Force:millinewtons"
      },
      {
        "LongName": "pounds-force",
        "Offset": 0,
        "Scale": 0.2248089430997105,
        "ShortName": "lbf",
        "id": "Force:pounds-force"
      },
      {
        "LongName": "kilonewtons",
        "Offset": 0,
        "Scale": 0.001,
        "ShortName": "kN",
        "id": "Force:kilonewtons"
      }
    ],
    "defaultUnit": "Force:newtons"
  },
  {
    "DimensionName": "Time",
    "LongName": "",
    "ShortName": "",
    "units": [
      {
        "LongName": "seconds",
        "Offset": 0,
        "Scale": 1,
        "ShortName": "s",
        "id": "Time:seconds"
      },
      {
        "LongName": "milliseconds",
        "Offset": 0,
        "Scale": 1000,
        "ShortName": "ms",
        "id": "Time:milliseconds"
      },
      {
        "LongName": "microseconds",
        "Offset": 0,
        "Scale": 1000000,
        "ShortName": "µs",
        "id": "Time:microseconds"
      }
    ],
    "defaultUnit": "Time:seconds"
  },
  {
    "DimensionName": "Torque",
    "LongName": "",
    "ShortName": "",
    "units": [
      {
        "LongName": "newton metres",
        "Offset": 0,
        "Scale": 1,
        "ShortName": "N⋅m",
        "id": "Torque:newton metres"
      },
      {
        "LongName": "newton centimetres",
        "Offset": 0,
        "Scale": 100,
        "ShortName": "N⋅cm",
        "id": "Torque:newton centimetres"
      },
      {
        "LongName": "pound-force-feet",
        "Offset": 0,
        "Scale": 0.7375621211696556,
        "ShortName": "lbf⋅ft",
        "id": "Torque:pound-force-feet"
      },
      {
        "LongName": "ounce-force-inches",
        "Offset": 0,
        "Scale": 141.61192726457386,
        "ShortName": "ozf⋅in",
        "id": "Torque:ounce-force-inches"
      }
    ],
    "defaultUnit": "Torque:newton metres"
  },
  {
    "DimensionName": "Inertia",
    "LongName": "",
    "ShortName": "",
    "units": [
      {
        "LongName": "grams",
        "Offset": 0,
        "Scale": 1,
        "ShortName": "g",
        "id": "Inertia:grams"
      },
      {
        "LongName": "kilograms",
        "Offset": 0,
        "Scale": 0.001,
        "ShortName": "kg",
        "id": "Inertia:kilograms"
      },
      {
        "LongName": "milligrams",
        "Offset": 0,
        "Scale": 1000,
        "ShortName": "mg",
        "id": "Inertia:milligrams"
      },
      {
        "LongName": "pounds",
        "Offset": 0,
        "Scale": 0.002204622621848776,
        "ShortName": "lb",
        "id": "Inertia:pounds"
      },
      {
        "LongName": "ounces",
        "Offset": 0,
        "Scale": 0.035273961949580414,
        "ShortName": "oz",
        "id": "Inertia:ounces"
      }
    ],
    "defaultUnit": "Inertia:grams"
  },
  {
    "DimensionName": "Rotational Inertia",
    "LongName": "",
    "ShortName": "",
    "units": [
      {
        "LongName": "gram-square metre",
        "Offset": 0,
        "Scale": 1,
        "ShortName": "g⋅m²",
        "id": "Rotational Inertia:gram-square metre"
      },
      {
        "LongName": "kilogram-square metre",
        "Offset": 0,
        "Scale": 0.001,
        "ShortName": "kg⋅m²",
        "id": "Rotational Inertia:kilogram-square metre"
      },
      {
        "LongName": "pound-square-feet",
        "Offset": 0,
        "Scale": 0.02373036,
        "ShortName": "lb⋅ft²",
        "id": "Rotational Inertia:pound-square-feet"
      }
    ],
    "defaultUnit": "Rotational Inertia:gram-square metre"
  },
  {
    "DimensionName": "Force Constant",
    "LongName": "",
    "ShortName": "",
    "units": [
      {
        "LongName": "newtons per amp",
        "Offset": 0,
        "Scale": 1,
        "ShortName": "N/A",
        "id": "Force Constant:newtons per amp"
      },
      {
        "LongName": "millinewtons per amp",
        "Offset": 0,
        "Scale": 1000,
        "ShortName": "mN/A",
        "id": "Force Constant:millinewtons per amp"
      },
      {
        "LongName": "kilonewtons per amp",
        "Offset": 0,
        "Scale": 0.001,
        "ShortName": "kN/A",
        "id": "Force Constant:kilonewtons per amp"
      },
      {
        "LongName": "pounds-force per amp",
        "Offset": 0,
        "Scale": 0.2248089430997105,
        "ShortName": "lbf/A",
        "id": "Force Constant:pounds-force per amp"
      }
    ],
    "defaultUnit": "Force Constant:newtons per amp"
  },
  {
    "DimensionName": "Torque Constant",
    "LongName": "",
    "ShortName": "",
    "units": [
      {
        "LongName": "newton metres per amp",
        "Offset": 0,
        "Scale": 1,
        "ShortName": "N⋅m/A",
        "id": "Torque Constant:newton metres per amp"
      },
      {
        "LongName": "millinewton metres per amp",
        "Offset": 0,
        "Scale": 1000,
        "ShortName": "mN⋅m/A",
        "id": "Torque Constant:millinewton metres per amp"
      },
      {
        "LongName": "kilonewton metres per amp",
        "Offset": 0,
        "Scale": 0.001,
        "ShortName": "kN⋅m/A",
        "id": "Torque Constant:kilonewton metres per amp"
      },
      {
        "LongName": "pound-force-feet per amp",
        "Offset": 0,
        "Scale": 0.7375621211696556,
        "ShortName": "lbf⋅ft/A",
        "id": "Torque Constant:pound-force-feet per amp"
      }
    ],
    "defaultUnit": "Torque Constant:newton metres per amp"
  },
  {
    "DimensionName": "Voltage",
    "LongName": "",
    "ShortName": "",
    "units": [
      {
        "LongName": "volts",
        "Offset": 0,
        "Scale": 1,
        "ShortName": "V",
        "id": "Voltage:volts"
      },
      {
        "LongName": "millivolts",
        "Offset": 0,
        "Scale": 1000,
        "ShortName": "mV",
        "id": "Voltage:millivolts"
      },
      {
        "LongName": "microvolts",
        "Offset": 0,
        "Scale": 1000000,
        "ShortName": "µV",
        "id": "Voltage:microvolts"
      }
    ],
    "defaultUnit": "Voltage:volts"
  },
  {
    "DimensionName": "Current Controller Proportional Gain",
    "LongName": "",
    "ShortName": "",
    "units": [
      {
        "LongName": "volts per amp",
        "Offset": 0,
        "Scale": 1,
        "ShortName": "V/A",
        "id": "Current Controller Proportional Gain:volts per amp"
      },
      {
        "LongName": "millivolts per amp",
        "Offset": 0,
        "Scale": 1000,
        "ShortName": "mV/A",
        "id": "Current Controller Proportional Gain:millivolts per amp"
      },
      {
        "LongName": "microvolts per amp",
        "Offset": 0,
        "Scale": 1000000,
        "ShortName": "µV/A",
        "id": "Current Controller Proportional Gain:microvolts per amp"
      }
    ],
    "defaultUnit": "Current Controller Proportional Gain:volts per amp"
  },
  {
    "DimensionName": "Current Controller Integral Gain",
    "LongName": "",
    "ShortName": "",
    "units": [
      {
        "LongName": "volts per amp per second",
        "Offset": 0,
        "Scale": 1,
        "ShortName": "V/(A⋅s)",
        "id": "Current Controller Integral Gain:volts per amp per second"
      },
      {
        "LongName": "millivolts per amp per second",
        "Offset": 0,
        "Scale": 1000,
        "ShortName": "mV/(A⋅s)",
        "id": "Current Controller Integral Gain:millivolts per amp per second"
      },
      {
        "LongName": "microvolts per amp per second",
        "Offset": 0,
        "Scale": 1000000,
        "ShortName": "µV/(A⋅s)",
        "id": "Current Controller Integral Gain:microvolts per amp per second"
      }
    ],
    "defaultUnit": "Current Controller Integral Gain:volts per amp per second"
  },
  {
    "DimensionName": "Current Controller Derivative Gain",
    "LongName": "",
    "ShortName": "",
    "units": [
      {
        "LongName": "volts second per amp",
        "Offset": 0,
        "Scale": 1,
        "ShortName": "V⋅s/A",
        "id": "Current Controller Derivative Gain:volts second per amp"
      },
      {
        "LongName": "millivolts second per amp",
        "Offset": 0,
        "Scale": 1000,
        "ShortName": "mV⋅s/A",
        "id": "Current Controller Derivative Gain:millivolts second per amp"
      },
      {
        "LongName": "microvolts second per amp",
        "Offset": 0,
        "Scale": 1000000,
        "ShortName": "µV⋅s/A",
        "id": "Current Controller Derivative Gain:microvolts second per amp"
      }
    ],
    "defaultUnit": "Current Controller Derivative Gain:volts second per amp"
  },
  {
    "DimensionName": "Resistance",
    "LongName": "",
    "ShortName": "",
    "units": [
      {
        "LongName": "kiloohms",
        "Offset": 0,
        "Scale": 0.001,
        "ShortName": "kΩ",
        "id": "Resistance:kiloohms"
      },
      {
        "LongName": "ohms",
        "Offset": 0,
        "Scale": 1,
        "ShortName": "Ω",
        "id": "Resistance:ohms"
      },
      {
        "LongName": "milliohms",
        "Offset": 0,
        "Scale": 1000,
        "ShortName": "mΩ",
        "id": "Resistance:milliohms"
      },
      {
        "LongName": "microohms",
        "Offset": 0,
        "Scale": 1000000,
        "ShortName": "µΩ",
        "id": "Resistance:microohms"
      },
      {
        "LongName": "nanoohms",
        "Offset": 0,
        "Scale": 1000000000,
        "ShortName": "nΩ",
        "id": "Resistance:nanoohms"
      }
    ],
    "defaultUnit": "Resistance:ohms"
  },
  {
    "DimensionName": "Inductance",
    "LongName": "",
    "ShortName": "",
    "units": [
      {
        "LongName": "henries",
        "Offset": 0,
        "Scale": 1,
        "ShortName": "H",
        "id": "Inductance:henries"
      },
      {
        "LongName": "millihenries",
        "Offset": 0,
        "Scale": 1000,
        "ShortName": "mH",
        "id": "Inductance:millihenries"
      },
      {
        "LongName": "microhenries",
        "Offset": 0,
        "Scale": 1000000,
        "ShortName": "µH",
        "id": "Inductance:microhenries"
      },
      {
        "LongName": "nanohenries",
        "Offset": 0,
        "Scale": 1000000000,
        "ShortName": "nH",
        "id": "Inductance:nanohenries"
      }
    ],
    "defaultUnit": "Inductance:henries"
  },
  {
    "DimensionName": "Voltage Constant",
    "LongName": "",
    "ShortName": "",
    "units": [
      {
        "LongName": "volt seconds per radian",
        "Offset": 0,
        "Scale": 1,
        "ShortName": "V·s/rad",
        "id": "Voltage Constant:volt seconds per radian"
      },
      {
        "LongName": "millivolt seconds per radian",
        "Offset": 0,
        "Scale": 1000,
        "ShortName": "mV·s/rad",
        "id": "Voltage Constant:millivolt seconds per radian"
      },
      {
        "LongName": "microvolt seconds per radian",
        "Offset": 0,
        "Scale": 1000000,
        "ShortName": "µV·s/rad",
        "id": "Voltage Constant:microvolt seconds per radian"
      }
    ],
    "defaultUnit": "Voltage Constant:volt seconds per radian"
  },
  {
    "DimensionName": "Absolute Temperature",
    "LongName": "",
    "ShortName": "",
    "units": [
      {
        "LongName": "degrees Celsius",
        "Offset": 0,
        "Scale": 1,
        "ShortName": "°C",
        "id": "Absolute Temperature:degrees Celsius"
      },
      {
        "LongName": "kelvins",
        "Offset": 273.15,
        "Scale": 1,
        "ShortName": "K",
        "id": "Absolute Temperature:kelvins"
      },
      {
        "LongName": "degrees Fahrenheit",
        "Offset": 32,
        "Scale": 1.8,
        "ShortName": "°F",
        "id": "Absolute Temperature:degrees Fahrenheit"
      },
      {
        "LongName": "degrees Rankine",
        "Offset": 491.67,
        "Scale": 1.8,
        "ShortName": "°R",
        "id": "Absolute Temperature:degrees Rankine"
      }
    ],
    "defaultUnit": "Absolute Temperature:degrees Celsius"
  },
  {
    "DimensionName": "Relative Temperature",
    "LongName": "",
    "ShortName": "",
    "units": [
      {
        "LongName": "degrees Celsius",
        "Offset": 0,
        "Scale": 1,
        "ShortName": "°C",
        "id": "Relative Temperature:degrees Celsius"
      },
      {
        "LongName": "kelvins",
        "Offset": 0,
        "Scale": 1,
        "ShortName": "K",
        "id": "Relative Temperature:kelvins"
      },
      {
        "LongName": "degrees Fahrenheit",
        "Offset": 0,
        "Scale": 1.8,
        "ShortName": "°F",
        "id": "Relative Temperature:degrees Fahrenheit"
      },
      {
        "LongName": "degrees Rankine",
        "Offset": 0,
        "Scale": 1.8,
        "ShortName": "°R",
        "id": "Relative Temperature:degrees Rankine"
      }
    ],
    "defaultUnit": "Relative Temperature:degrees Celsius"
  },
  {
    "DimensionName": "Frequency",
    "LongName": "",
    "ShortName": "",
    "units": [
      {
        "LongName": "gigahertz",
        "Offset": 0,
        "Scale": 1e-9,
        "ShortName": "GHz",
        "id": "Frequency:gigahertz"
      },
      {
        "LongName": "megahertz",
        "Offset": 0,
        "Scale": 0.000001,
        "ShortName": "MHz",
        "id": "Frequency:megahertz"
      },
      {
        "LongName": "kilohertz",
        "Offset": 0,
        "Scale": 0.001,
        "ShortName": "kHz",
        "id": "Frequency:kilohertz"
      },
      {
        "LongName": "hertz",
        "Offset": 0,
        "Scale": 1,
        "ShortName": "Hz",
        "id": "Frequency:hertz"
      },
      {
        "LongName": "millihertz",
        "Offset": 0,
        "Scale": 1000,
        "ShortName": "mHz",
        "id": "Frequency:millihertz"
      },
      {
        "LongName": "microhertz",
        "Offset": 0,
        "Scale": 1000000,
        "ShortName": "µHz",
        "id": "Frequency:microhertz"
      },
      {
        "LongName": "nanohertz",
        "Offset": 0,
        "Scale": 1000000000,
        "ShortName": "nHz",
        "id": "Frequency:nanohertz"
      }
    ],
    "defaultUnit": "Frequency:hertz"
  }
];

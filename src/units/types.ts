export interface DimensionDefinition {
  DimensionName: string;
  LongName: string;
  ShortName: string;
  units: UnitDefinition[];
  defaultUnit: string;
}

export interface UnitDefinition {
  id: string;
  ShortName: string;
  LongName: string;
  Offset: number;
  Scale: number;
}

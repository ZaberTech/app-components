import { inject, injectable } from 'inversify';
import {
  CreateVirtualDeviceChainParams,
  DeviceProvisioningParams,
  VirtualDeviceChainProperties,
  VirtualDeviceChainState,
  VirtualDevicePowerSwitchParams,
  VirtualDeviceRenameChainParams,
  VirtualDeviceExistence,
  VirtualDeviceProduct,
  VirtualDeviceFirmwareVersion,
} from '@zaber/serverless-service-definitions';
import { Tools } from '@zaber/motion';
import _ from 'lodash';
import { ensureError } from '@zaber/toolbox';

import { ConnectionError, HttpError, Request, RequestOptions } from '../request';
import { Config } from '../config';
import { AuthRequest } from '../request/auth_request';

import * as Schema from './schema';
import { ApiError } from './error';
import { AccessTokenCreationArguments, FirmwareVersion } from './types';

const zaberWebsiteUrl = 'https://www.zaber.com';
const zaberWebsiteApiUrl = `${zaberWebsiteUrl}/api`;

export const DEVICE_DB_API_VERSION = '1.10';
const dbPipePath = _.memoize(Tools.getDbServicePipePath);

export { AVAILABLE_ACCESS_TOKEN_SCOPES, AVAILABLE_ACCESS_TOKEN_CREATED_BY, AccessTokenScope, AccessTokenCreatedBy } from './schema';

interface DeviceInfoRequest {
  deviceID: number;
  fwVersion?: FirmwareVersion;
  modified?: boolean;
  peripherals?: {
    peripheralID: number;
    modified?: boolean;
  }[];
}

@injectable()
export class ZaberApi {
  static Error = ApiError;
  public verboseLogs = false;

  get deviceDB(): string {
    return Config.current.zaber.deviceDB ?? 'public';
  }
  get apiUrl(): string {
    if (!Config.current.zaber.apiUrl) {
      throw new Error('apiUrl is not provided');
    }
    return Config.current.zaber.apiUrl;
  }

  constructor(
    @inject(Request) private readonly request: Request,
    @inject(AuthRequest) private readonly authRequest: AuthRequest,
  ) {
  }

  /**
   * Makes a call to the api endpoint, or, if offline, it will connect to the piped server instead
   * @param url The url to send the request to (not including prefix)
   * @param method The HTTP method
   * @param body Body of POST request
   * @param options Same as Request options
   */
  private async dbRequest<Return = unknown>(
    url: `/${string}`, method?: string | null, body?: unknown, options?: RequestOptions
  ): Promise<Return> {
    if (Config.current.zaber.deviceDB === 'offline') {
      const { pipedRequest } = await import('../request/pipedRequest');
      return await pipedRequest<Return>(dbPipePath(), url, method, body, options);
    } else {
      return await this.request.request<Return>(`${this.apiUrl}/device-db/${this.deviceDB}${url}`, method, body, options);
    }
  }

  private static processError(err: Error): ZaberApi.Error {
    if (err instanceof HttpError || err instanceof ConnectionError) {
      return new ZaberApi.Error(err.message, err);
    }
    return new ZaberApi.Error(`Cannot perform request: ${err}`);
  }

  private static deviceDbSearchParams() {
    const search = new URLSearchParams();
    search.append('api-version', DEVICE_DB_API_VERSION);
    return search;
  }

  public async getDeviceInfo(request: DeviceInfoRequest): Promise<Schema.DeviceInfo> {
    const search = ZaberApi.deviceDbSearchParams();
    if (request.fwVersion) {
      const { fwVersion } = request;
      search.append('fw', `${fwVersion.major}.${fwVersion.minor}.${fwVersion.build}`);
    }
    if (request.modified) {
      search.append('modified', '1');
    }
    const somePeripheralModified = request.peripherals?.some(peripheral => peripheral.modified === true);
    request.peripherals?.forEach(peripheral => {
      search.append('peripheral', `${peripheral.peripheralID}`);
      if (somePeripheralModified) {
        search.append('peripheral-modified', `${peripheral.modified ? 1 : 0}`);
      }
    });

    try {
      const info = await this.dbRequest<Schema.DeviceInfo>(`/device/${request.deviceID}`, null, null, { search });

      return info;
    } catch (err) {
      throw ZaberApi.processError(ensureError(err));
    }
  }

  public async getSupportedPeripherals(productID: number): Promise<Schema.SupportedPeripherals> {
    const search = ZaberApi.deviceDbSearchParams();
    try {
      const info = await this.dbRequest<Schema.SupportedPeripherals>(
        `/product/${productID}/supported-peripherals`, null, null, { search }
      );

      return info;
    } catch (err) {
      throw ZaberApi.processError(ensureError(err));
    }
  }

  public async getServoTuningInfo(productID: number): Promise<Schema.ServoTuningInfo> {
    const search = ZaberApi.deviceDbSearchParams();
    try {
      const info = await this.dbRequest<Schema.ServoTuningInfo>(`/product/${productID}/servo-tuning-info`, null, null, { search });

      return info;
    } catch (err) {
      throw ZaberApi.processError(ensureError(err));
    }
  }

  public async searchProduct({
    deviceOrPeripheralId,
    nameLikeExpression,
    fwVersion,
    limit,
  }: {
    deviceOrPeripheralId?: number;
    nameLikeExpression?: string;
    fwVersion?: Partial<FirmwareVersion>;
    limit?: number;
  }): Promise<Schema.ProductSearch['products']> {
    const search = ZaberApi.deviceDbSearchParams();
    if (nameLikeExpression) {
      search.set('name', nameLikeExpression);
    } else if (deviceOrPeripheralId) {
      search.set('device-id', String(deviceOrPeripheralId));
    }

    let fwString = '';
    if (fwVersion?.major != null) {
      fwString += `${fwVersion.major}`;
      if (fwVersion.minor != null) {
        fwString += `.${fwVersion.minor}`;
        if (fwVersion.build != null) {
          fwString += `.${fwVersion.build}`;
        }
      }
    }
    if (fwString) {
      search.set('fw', fwString);
    }

    if (limit) { search.set('limit', `${limit}`) }

    try {
      const result = await this.dbRequest<Schema.ProductSearch>('/product/search', null, null, { search });
      return result.products;
    } catch (err) {
      throw ZaberApi.processError(ensureError(err));
    }
  }

  private generateZaberWebsiteSearchQuery(deviceIds: number[], peripheralIds?: number[]): URLSearchParams {
    const search = new URLSearchParams();
    if (deviceIds.length > 0) {
      search.append('deviceIds', deviceIds.join(','));
    }
    if (peripheralIds && peripheralIds.length > 0) {
      search.append('peripheralIds', peripheralIds.join(','));
    }
    return search;
  }

  public async getWebsiteDataForProducts(deviceIds: number[], peripheralIds?: number[]): Promise<Schema.WebsiteProduct[]> {
    try {
      const search = this.generateZaberWebsiteSearchQuery(deviceIds, peripheralIds);
      const info = await this.request.request<Schema.WebsiteProduct[]>(`${zaberWebsiteApiUrl}/part-details`, null, null, { search });
      return info;
    } catch (err) {
      throw ZaberApi.processError(ensureError(err));
    }
  }

  public async getWebsiteImageForProducts(deviceIds: number[], peripheralIds?: number[]): Promise<Schema.WebsiteProductImage[]> {
    if (deviceIds.length === 0 && (!peripheralIds || peripheralIds.length === 0)) {
      return [];
    }
    try {
      const search = this.generateZaberWebsiteSearchQuery(deviceIds, peripheralIds);
      const images = await this.request.request<Schema.WebsiteProductImage[]>(`${zaberWebsiteApiUrl}/part-images`, null, null, { search });
      return images;
    } catch (err) {
      throw ZaberApi.processError(ensureError(err));
    }
  }

  public async sendFeedback(body: {
    product: string;
    version: string;
    comment: string;
    experience?: 'OK' | 'NOK';
    meta?: unknown;
  }
  ): Promise<void> {
    return this.request.request<void>(`${this.apiUrl}/feedback`, 'POST', body);
  }

  async getUserAccessTokens(): Promise<Schema.AccessTokenData[]> {
    const { tokens } = await this.authRequest.request<Schema.AccessTokenGetResponse>(`${this.apiUrl}/access-token`, 'GET');
    return tokens;
  }

  async createUserAccessToken(optionalData?: AccessTokenCreationArguments): Promise<Schema.AccessTokenCreationResponse> {
    return await this.authRequest.request<Schema.AccessTokenCreationResponse>(`${this.apiUrl}/access-token`, 'POST', optionalData);
  }

  async deleteUserAccessToken(tokenId: string): Promise<void> {
    return await this.authRequest.request(`${this.apiUrl}/access-token/${tokenId}`, 'DELETE');
  }


  async listVirtualDeviceProducts(modelVersion: number): Promise<VirtualDeviceProduct[]> {
    try {
      return await this.request.request<VirtualDeviceProduct[]>(
        `${this.apiUrl}/virtual-device-public/products/list?modelVersion=${modelVersion}`, 'GET');
    } catch (err) {
      throw ZaberApi.processError(ensureError(err));
    }
  }


  async listVirtualDeviceFirmwareVersions(): Promise<VirtualDeviceFirmwareVersion[]> {
    try {
      return await this.request.request<VirtualDeviceFirmwareVersion[]>(
        `${this.apiUrl}/virtual-device-public/products/fw/version`, 'GET');
    } catch (err) {
      throw ZaberApi.processError(ensureError(err));
    }
  }


  async createVirtualDeviceChain(
    chainConfig: DeviceProvisioningParams[],
    name: string,
    createdBy: string,
    isPublic: boolean = false): Promise<VirtualDeviceChainState> {
    const options: CreateVirtualDeviceChainParams = {
      devices: chainConfig,
      name,
      createdBy,
    };

    if (this.verboseLogs) {
      options.logDebugOverride = '*,-mqtt*';
    }

    try {
      if (isPublic) {
        return await this.request.request<VirtualDeviceChainState>(`${this.apiUrl}/virtual-device-public`, 'POST', options);
      } else {
        return await this.authRequest.request<VirtualDeviceChainState>(`${this.apiUrl}/virtual-device`, 'POST', options);
      }
    } catch (err) {
      throw ZaberApi.processError(ensureError(err));
    }
  }


  async renameVirtualDeviceChain(cloudId: string, name: string): Promise<void> {
    const options: VirtualDeviceRenameChainParams = {
      cloudId,
      name,
    };

    try {
      await this.authRequest.request<VirtualDeviceRenameChainParams>(`${this.apiUrl}/virtual-device/rename`, 'POST', options);
    } catch (err) {
      throw ZaberApi.processError(ensureError(err));
    }
  }


  async deleteVirtualDeviceChain(cloudId: string, isPublic: boolean = false): Promise<VirtualDeviceChainState> {
    try {
      if (isPublic) {
        return await this.request.request<VirtualDeviceChainState>(`${this.apiUrl}/virtual-device-public/${cloudId}`, 'DELETE');
      } else {
        return await this.authRequest.request<VirtualDeviceChainState>(`${this.apiUrl}/virtual-device/${cloudId}`, 'DELETE');
      }
    } catch (err) {
      throw ZaberApi.processError(ensureError(err));
    }
  }


  async cleanupDeletedUserVirtualDevices(): Promise<void> {
    try {
      await this.authRequest.request<VirtualDeviceChainState>(`${this.apiUrl}/virtual-device/housekeeping/user`, 'DELETE');
    } catch (err) {
      throw ZaberApi.processError(ensureError(err));
    }
  }


  async listVirtualDeviceChains(): Promise<VirtualDeviceChainProperties[]> {
    try {
      // Only listing private simulations is allowed.
      return await this.authRequest.request<VirtualDeviceChainProperties[]>(`${this.apiUrl}/virtual-device`);
    } catch (err) {
      throw ZaberApi.processError(ensureError(err));
    }
  }


  async getVirtualDeviceChain(cloudId: string, isPublic: boolean = false): Promise<VirtualDeviceChainProperties> {
    try {
      if (isPublic) {
        return await this.request.request<VirtualDeviceChainProperties>(`${this.apiUrl}/virtual-device-public/${cloudId}`);
      } else {
        return await this.authRequest.request<VirtualDeviceChainProperties>(`${this.apiUrl}/virtual-device/${cloudId}`);
      }
    } catch (err) {
      throw ZaberApi.processError(ensureError(err));
    }
  }


  async virtualDevicePowerSwitch(cloudId: string, newState: boolean, isPublic: boolean = false): Promise<VirtualDeviceChainState> {
    const options: VirtualDevicePowerSwitchParams = {
      cloudId,
      power: newState,
      startedBy: 'website'
    };

    if (this.verboseLogs) {
      options.logDebugOverride = '*,-mqtt*';
    }

    try {
      if (isPublic) {
        return await this.request.request<VirtualDeviceChainState>(`${this.apiUrl}/virtual-device-public/power`, 'POST', options);
      } else {
        return await this.authRequest.request<VirtualDeviceChainState>(`${this.apiUrl}/virtual-device/power`, 'POST', options);
      }
    } catch (err) {
      throw ZaberApi.processError(ensureError(err));
    }
  }

  async virtualDeviceExists(cloudId: string): Promise<VirtualDeviceExistence> {
    try {
      return await this.request.request<VirtualDeviceExistence>(`${this.apiUrl}/virtual-device-public/${cloudId}/exists`);
    } catch (err) {
      throw ZaberApi.processError(ensureError(err));
    }
  }
}

// eslint-disable-next-line @typescript-eslint/no-namespace
export namespace ZaberApi {
  export type Error = ApiError;
  export type DeviceInfo = Schema.DeviceInfo;
  export type SupportedPeripherals = Schema.SupportedPeripherals;
  export type WebsiteProduct = Schema.WebsiteProduct;
  export type WebsiteProductImage = Schema.WebsiteProductImage;
  export type AccessTokenData = Schema.AccessTokenData;
  export type AccessTokenCreationData = Schema.AccessTokenCreationResponse;
  export type ProductSearch = Schema.ProductSearch;
}

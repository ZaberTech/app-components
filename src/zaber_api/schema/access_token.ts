export const AVAILABLE_ACCESS_TOKEN_SCOPES = (['iot'] as const).slice();
export const AVAILABLE_ACCESS_TOKEN_CREATED_BY = (['zaberLauncher', 'virtualDevice'] as const).slice();

export type AccessTokenScope = typeof AVAILABLE_ACCESS_TOKEN_SCOPES[number];
export type AccessTokenCreatedBy = typeof AVAILABLE_ACCESS_TOKEN_CREATED_BY[number];

export interface AccessTokenData {
  /**
   * ID of the token. Required for running API operations on the given token
   *
   * NOTE: This is NOT the access token value itself.
   */
  tokenId: string;
  /**
   * The ISO-8601 formatted time at which the token was created
   */
  createdAt: string;
  /**
   * The scopes for which this token is valid. If empty, then it is valid for all scopes
   */
  scopes?: AccessTokenScope[];
  /**
   * The ISO-8601 formatted time at which this token will expire. If empty, then the token has
   * no expiry date
   */
  expiry?: string;
  /**
   * An arbitrary description for the token
   */
  description?: string;
  /**
   * The ISO-8601 formatted time at which the token was last used for getting user data
   */
  lastUsedAt?: string;
  /**
   * Some data about who last used the token. It will likely contain an IP address. But can also
   * be empty
   */
  lastUsedBy?: string;
  /**
   * Defines the purpose of the token and why it was created. If empty, then the token is
   * considered to have been created by the user directly
   */
  createdBy?: AccessTokenCreatedBy;
  /**
   * The ISO-8601 formatted time at which the token was deleted. If empty, then this token is not
   * considered as "deleted"
   */
  deletedAt?: string;
}

export interface AccessTokenCreationResponse {
  /**
   * ID of the token. Required for running API operations on the given token
   *
   * NOTE: This is NOT the access token value itself.
   */
  tokenId: string;
  /**
   * The token value itself. This is the value that users must use in order to authenticate
   * themselves for Zaber services using access tokens.
   *
   * NOTE: The API only returns tokens just after they get created. Token values can never be
   * obtained again after that.
   */
  token: string;
}

export interface AccessTokenGetResponse {
  /**
   * A given user's access tokens
   */
  tokens: AccessTokenData[];
}

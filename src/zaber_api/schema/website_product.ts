export interface WebsiteProduct {
  /**
   * id of the product. Can be a deviceId or PeripheralId
   */
  id: number;
  /**
   * Path to the product page on the Zaber website
   */
  url: string;
  /**
   * Full title used for the product on the Zaber website
   */
  title: string;
}

export interface WebsiteProductImage {
  /**
   * id of the product. Can be a deviceId or PeripheralId
   */
  id: number;
  /**
   * Path to a thumbnail image for this product
   */
  thumbnail: string;
  /**
   * Path to the full image
   */
  image: string;
}

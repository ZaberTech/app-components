import { injectable } from 'inversify';
import { DeviceProvisioningParams } from '@zaber/serverless-service-definitions';

import { HttpError, Request } from '../request';
import { createContainer, destroyContainer } from '../test';
import { Config } from '../config';
import { AuthRequest } from '../request/auth_request';

import { ZaberApi, DEVICE_DB_API_VERSION } from '.';

let container: ReturnType<typeof createContainer>;
let request: RequestMock;
let authRequest: AuthRequestMock;

@injectable()
class RequestMock {
  request = jest.fn().mockResolvedValue({});
}

@injectable()
class AuthRequestMock {
  request = jest.fn().mockResolvedValue({});
}

beforeAll(() => {
  Config.merge({
    zaber: {
      apiUrl: 'https://api.test.io',
      deviceDB: 'master',
    },
  });
});

let api: ZaberApi;

beforeEach(() => {
  container = createContainer();
  container.bind<unknown>(Request).to(RequestMock);
  container.bind<unknown>(AuthRequest).to(AuthRequestMock);

  request = container.get<unknown>(Request) as RequestMock;
  authRequest = container.get<unknown>(AuthRequest) as AuthRequestMock;
  api = container.get(ZaberApi);
});

afterEach(() => {
  jest.clearAllMocks();

  destroyContainer();
  container = null!;

  api = null!;
  request = null!;
});

describe('getDeviceInfo', () => {
  test('formats the URL correctly (integrated)', async () => {
    await api.getDeviceInfo({ deviceID: 1234, fwVersion: { major: 1, minor: 2, build: 3 } });
    expect(request.request).toHaveBeenCalledTimes(1);
    const args = request.request.mock.calls[0];
    expect(args[0]).toBe('https://api.test.io/device-db/master/device/1234');
    expect(args[3].search.toString()).toBe([
      `api-version=${DEVICE_DB_API_VERSION}`,
      'fw=1.2.3',
    ].join('&'));
  });

  test('formats the URL correctly (controller)', async () => {
    await api.getDeviceInfo({
      deviceID: 1234,
      fwVersion: { major: 1, minor: 2, build: 0 },
      peripherals: [{ peripheralID: 1111 }, { peripheralID: 2222 }]
    });
    expect(request.request).toHaveBeenCalledTimes(1);
    const args = request.request.mock.calls[0];
    expect(args[0]).toBe('https://api.test.io/device-db/master/device/1234');
    expect(args[3].search.toString()).toBe([
      `api-version=${DEVICE_DB_API_VERSION}`,
      'fw=1.2.0',
      'peripheral=1111',
      'peripheral=2222',
    ].join('&'));
  });

  test('formats the URL correctly (controller, modified)', async () => {
    await api.getDeviceInfo({
      deviceID: 1234,
      modified: true,
      fwVersion: { major: 1, minor: 2, build: 0 },
      peripherals: [{ peripheralID: 1111, modified: true }, { peripheralID: 2222 }]
    });
    expect(request.request).toHaveBeenCalledTimes(1);
    const args = request.request.mock.calls[0];
    expect(args[3].search.toString()).toBe([
      `api-version=${DEVICE_DB_API_VERSION}`,
      'fw=1.2.0',
      'modified=1',
      'peripheral=1111',
      'peripheral-modified=1',
      'peripheral=2222',
      'peripheral-modified=0',
    ].join('&'));
  });
});

describe('errors', () => {
  test('handles HTTP error', async () => {
    request.request.mockRejectedValueOnce(new HttpError('Request error: 404', 404, 'Cannot find device'));

    const promise = api.getDeviceInfo({ deviceID: 1234 });
    const error = await promise.catch(e => e);
    expect(error instanceof ZaberApi.Error).toBeTruthy();
    expect(error.message).toBe('Request error: 404');
    expect((error as ZaberApi.Error).innerError).toBeInstanceOf(HttpError);
  });

  test('handles other errors', async () => {
    request.request.mockRejectedValueOnce(new Error('No internet'));

    const promise = api.getDeviceInfo({ deviceID: 1234 });
    const error = await promise.catch(e => e);
    expect(error instanceof ZaberApi.Error).toBeTruthy();
    expect(error.message).toBe('Cannot perform request: Error: No internet');
  });
});

describe('getSupportedPeripherals', () => {
  test('formats the URL correctly and returns data', async () => {
    request.request.mockResolvedValueOnce('data');
    const data = await api.getSupportedPeripherals(1234);

    expect(request.request).toHaveBeenCalledTimes(1);
    const args = request.request.mock.calls[0];
    expect(args[0]).toBe('https://api.test.io/device-db/master/product/1234/supported-peripherals');
    expect(args[3].search.toString()).toBe(`api-version=${DEVICE_DB_API_VERSION}`);
    expect(data).toBe('data');
  });
});

describe('getServoTuningInfo', () => {
  test('formats the URL correctly and returns data', async () => {
    request.request.mockResolvedValueOnce('data');
    const data = await api.getServoTuningInfo(1234);

    expect(request.request).toHaveBeenCalledTimes(1);
    const args = request.request.mock.calls[0];
    expect(args[0]).toBe('https://api.test.io/device-db/master/product/1234/servo-tuning-info');
    expect(args[3].search.toString()).toBe(`api-version=${DEVICE_DB_API_VERSION}`);
    expect(data).toBe('data');
  });
});

describe('getWebsiteDataForProducts', () => {
  test('formats the URL correctly and returns data', async () => {
    request.request.mockResolvedValueOnce('data');
    const data = await api.getWebsiteDataForProducts([50021, 50909]);
    expect(request.request).toHaveBeenCalledTimes(1);

    const args = request.request.mock.calls[0];
    expect(args[0]).toBe('https://www.zaber.com/api/part-details');
    expect(args[3].search.toString()).toBe('deviceIds=50021%2C50909');
    expect(data).toBe('data');
  });

  test('formats the URL correctly and returns image data', async () => {
    request.request.mockResolvedValueOnce('data');
    const data = await api.getWebsiteImageForProducts([50021, 50909], [66207, 70267, 46355]);
    expect(request.request).toHaveBeenCalledTimes(1);

    const args = request.request.mock.calls[0];
    expect(args[0]).toBe('https://www.zaber.com/api/part-images');
    expect(args[3].search.toString()).toBe('deviceIds=50021%2C50909&peripheralIds=66207%2C70267%2C46355');
    expect(data).toBe('data');
  });

  test('does not call endpoint for an empty list of ids', async () => {
    request.request.mockResolvedValueOnce('data');
    await api.getWebsiteImageForProducts([], []);
    await api.getWebsiteImageForProducts([]);
    expect(request.request).toHaveBeenCalledTimes(0);
  });
});

test('Sends feedback to our feedback endpoint', async () => {
  request.request.mockResolvedValueOnce('data');
  const product = 'Launcher';
  const version = '0.0';
  const comment = 'feedback';
  await api.sendFeedback({ product, version, comment });
  expect(request.request).toHaveBeenCalledTimes(1);
  expect(request.request).toHaveBeenCalledWith('https://api.test.io/feedback', 'POST', { product, version, comment });
});

describe('Access token API', () => {
  test('get list of tokens', async () => {
    authRequest.request.mockResolvedValueOnce({ tokens: ['some token'] });
    const result = await api.getUserAccessTokens();
    expect(result).toEqual(['some token']);
    expect(authRequest.request.mock.calls[0][1]).toEqual('GET');
  });

  test('create a token', async () => {
    const data = { description: 'some description' };
    const expectedResult = { token: 'abc', tokenId: '123' };

    authRequest.request.mockResolvedValueOnce(expectedResult);
    const result = await api.createUserAccessToken(data);

    expect(result).toEqual(expectedResult);
    expect(authRequest.request.mock.calls[0][1]).toEqual('POST');
    expect(authRequest.request.mock.calls[0][2]).toEqual(data);
  });

  test('delete a token', async () => {
    const tokenId = '123';
    authRequest.request.mockResolvedValueOnce(undefined);
    await api.deleteUserAccessToken(tokenId);
    expect(authRequest.request.mock.calls[0][0]).toEqual('https://api.test.io/access-token/123');
    expect(authRequest.request.mock.calls[0][1]).toEqual('DELETE');
  });
});


describe('Virtual Device API', () => {
  describe('listVirtualDeviceProducts', () => {
    it('calls the URL correctly', async () => {
      await api.listVirtualDeviceProducts(1);
      expect(request.request).toHaveBeenCalledTimes(1);
      const args = request.request.mock.calls[0];
      expect(args[0]).toBe('https://api.test.io/virtual-device-public/products/list?modelVersion=1');
      expect(args[1]).toEqual('GET');
    });
  });


  describe('createVirtualDeviceChain', () => {
    it('calls the URL correctly (private)', async () => {
      const provisioning: DeviceProvisioningParams[] = [
        { deviceId: 12345 },
        { deviceId: 23456 },
      ];
      await api.createVirtualDeviceChain(provisioning, 'My Devices', 'Me', false);
      expect(authRequest.request).toHaveBeenCalledTimes(1);
      const args = authRequest.request.mock.calls[0];
      expect(args[0]).toBe('https://api.test.io/virtual-device');
      expect(args[1]).toEqual('POST');
      expect(args[2]).toEqual({
        devices: provisioning,
        name: 'My Devices',
        createdBy: 'Me',
      });
    });

    it('calls the URL correctly (public)', async () => {
      const provisioning: DeviceProvisioningParams[] = [
        { deviceId: 12345 },
        { deviceId: 23456 },
      ];
      await api.createVirtualDeviceChain(provisioning, 'My Devices', 'Me', true);
      expect(request.request).toHaveBeenCalledTimes(1);
      const args = request.request.mock.calls[0];
      expect(args[0]).toBe('https://api.test.io/virtual-device-public');
      expect(args[1]).toEqual('POST');
      expect(args[2]).toEqual({
        devices: provisioning,
        name: 'My Devices',
        createdBy: 'Me',
      });
    });

    it('handles HTTP errors', async () => {
      request.request.mockRejectedValueOnce(new HttpError('Request error: 400', 400, 'Too many devices'));
      const error = await api.createVirtualDeviceChain([{ deviceId: 23456 }], 'My Devices', 'Me', true).catch(e => e);
      expect(error instanceof ZaberApi.Error).toBeTruthy();
      expect(error.message).toBe('Request error: 400');
    });
  });


  describe('deleteVirtualDeviceChain', () => {
    it('calls the URL correctly (private)', async () => {
      await api.deleteVirtualDeviceChain('12345', false);
      expect(authRequest.request).toHaveBeenCalledTimes(1);
      const args = authRequest.request.mock.calls[0];
      expect(args[0]).toBe('https://api.test.io/virtual-device/12345');
      expect(args[1]).toEqual('DELETE');
    });

    it('calls the URL correctly (public)', async () => {
      await api.deleteVirtualDeviceChain('12345', true);
      expect(request.request).toHaveBeenCalledTimes(1);
      const args = request.request.mock.calls[0];
      expect(args[0]).toBe('https://api.test.io/virtual-device-public/12345');
      expect(args[1]).toEqual('DELETE');
    });

    it('handles HTTP errors', async () => {
      request.request.mockRejectedValueOnce(new HttpError('Request error: 400', 400, 'Invalid ID'));
      const error = await api.deleteVirtualDeviceChain('12345', true).catch(e => e);
      expect(error instanceof ZaberApi.Error).toBeTruthy();
      expect(error.message).toBe('Request error: 400');
    });
  });


  describe('cleanupDeletedUserVirtualDevices', () => {
    it('calls the URL correctly (private)', async () => {
      await api.cleanupDeletedUserVirtualDevices();
      expect(authRequest.request).toHaveBeenCalledTimes(1);
      const args = authRequest.request.mock.calls[0];
      expect(args[0]).toBe('https://api.test.io/virtual-device/housekeeping/user');
      expect(args[1]).toEqual('DELETE');
    });
  });


  describe('listVirtualDeviceChains', () => {
    it('calls the URL correctly', async () => {
      await api.listVirtualDeviceChains();
      expect(authRequest.request).toHaveBeenCalledTimes(1);
      const args = authRequest.request.mock.calls[0];
      expect(args[0]).toBe('https://api.test.io/virtual-device');
    });

    it('handles HTTP errors', async () => {
      authRequest.request.mockRejectedValueOnce(new HttpError('Request error: 400', 400, 'Invalid user ID'));
      const error = await api.listVirtualDeviceChains().catch(e => e);
      expect(error instanceof ZaberApi.Error).toBeTruthy();
      expect(error.message).toBe('Request error: 400');
    });
  });


  describe('getVirtualDeviceChain', () => {
    it('calls the URL correctly (private)', async () => {
      await api.getVirtualDeviceChain('12345', false);
      expect(authRequest.request).toHaveBeenCalledTimes(1);
      const args = authRequest.request.mock.calls[0];
      expect(args[0]).toBe('https://api.test.io/virtual-device/12345');
    });

    it('calls the URL correctly (public)', async () => {
      await api.getVirtualDeviceChain('12345', true);
      expect(request.request).toHaveBeenCalledTimes(1);
      const args = request.request.mock.calls[0];
      expect(args[0]).toBe('https://api.test.io/virtual-device-public/12345');
    });

    it('handles HTTP errors', async () => {
      request.request.mockRejectedValueOnce(new HttpError('Request error: 400', 400, 'Invalid ID'));
      const error = await api.getVirtualDeviceChain('12345', true).catch(e => e);
      expect(error instanceof ZaberApi.Error).toBeTruthy();
      expect(error.message).toBe('Request error: 400');
    });
  });


  describe('virtualDevicePowerSwitch', () => {
    it('calls the URL correctly (private)', async () => {
      await api.virtualDevicePowerSwitch('12345', true, false);
      expect(authRequest.request).toHaveBeenCalledTimes(1);
      const args = authRequest.request.mock.calls[0];
      expect(args[0]).toBe('https://api.test.io/virtual-device/power');
      expect(args[1]).toEqual('POST');
      expect(args[2]).toEqual({
        cloudId: '12345',
        power: true,
        startedBy: 'website',
      });
    });

    it('calls the URL correctly (public)', async () => {
      await api.virtualDevicePowerSwitch('12345', false, true);
      expect(request.request).toHaveBeenCalledTimes(1);
      const args = request.request.mock.calls[0];
      expect(args[0]).toBe('https://api.test.io/virtual-device-public/power');
      expect(args[1]).toEqual('POST');
      expect(args[2]).toEqual({
        cloudId: '12345',
        power: false,
        startedBy: 'website',
      });
    });

    it('handles HTTP errors', async () => {
      request.request.mockRejectedValueOnce(new HttpError('Request error: 400', 400, 'Invalid ID'));
      const error = await api.virtualDevicePowerSwitch('12345', false, true).catch(e => e);
      expect(error instanceof ZaberApi.Error).toBeTruthy();
      expect(error.message).toBe('Request error: 400');
    });
  });

  describe('searchProduct', () => {
    it('formats the URL correctly', async () => {
      await api.searchProduct({
        nameLikeExpression: 'X-LSM%',
        fwVersion: { major: 7, minor: 28 },
        limit: 5,
      });
      const args = request.request.mock.calls[0];
      expect(args[0]).toBe('https://api.test.io/device-db/master/product/search');
      expect(args[3].search.toString()).toBe(`api-version=${DEVICE_DB_API_VERSION}&name=X-LSM%25&fw=7.28&limit=5`);

      await api.searchProduct({
        deviceOrPeripheralId: 1234,
        fwVersion: { major: 7 },
      });
      const args2 = request.request.mock.calls[1];
      expect(args2[3].search.toString()).toBe(`api-version=${DEVICE_DB_API_VERSION}&device-id=1234&fw=7`);
    });
  });
});

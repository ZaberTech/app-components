import { AccessTokenData } from './schema';

export interface FirmwareVersion {
  major: number;
  minor: number;
  build: number;
}

export type AccessTokenCreationArguments = Partial<Pick<AccessTokenData, 'description' | 'expiry' | 'scopes' | 'createdBy'>>;

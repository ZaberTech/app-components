import { ConnectionError, HttpError } from '../request';

type InnerErrors = HttpError | ConnectionError;

export class ApiError extends Error {
  constructor(
    message: string,
    readonly innerError?: InnerErrors,
  ) {
    super(message);
    Object.setPrototypeOf(this, ApiError.prototype);
  }
}

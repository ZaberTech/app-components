/* eslint-disable no-console */
const { getLatestDatabaseFile } = require('@zaber/tools');
const fs = require('fs');
const _ = require('lodash');
const path = require('path');
const ejs = require('ejs');
const sql = require('sqlite3');

function getDimensionsData(db) {
  return new Promise((resolve, reject) =>
    db.all('SELECT DimensionName, Id, LongName, ShortName FROM Data_Dimensions ORDER BY Id;', [], (err, rows) => {
      if (err) {
        reject(err);
      }
      resolve(rows);
    })
  );
}

function getUnitData(db) {
  return new Promise((resolve, reject) =>
    db.all('SELECT DimensionId, LongName, Offset, Scale, ShortName FROM Data_Units;', [], (err, rows) => {
      if (err) {
        reject(err);
      }
      resolve(rows);
    })
  );
}

async function getDataForUnits() {
  const dbPath = await getLatestDatabaseFile();
  const db = new sql.Database(dbPath, sql.OPEN_READONLY);

  try {
    const dimensionData = await getDimensionsData(db);
    const unitData = await getUnitData(db);

    return { dimensionsRaw: dimensionData, units: unitData };
  } finally {
    db.close();
  }
}

async function genUnits(outputFile) {
  const { dimensionsRaw, units } = await getDataForUnits();

  const unitsByDimensions = _.groupBy(units, unit => unit.DimensionId);
  const dimensions = dimensionsRaw.map(dimension => {
    const dimensionDef = ({
      ..._.omit(dimension, 'Id'),
      units: unitsByDimensions[dimension.Id].map(unit => ({
        ..._.omit(unit, 'DimensionId'),
        id: `${dimension.DimensionName}:${unit.LongName}`
      })),
    });
    dimensionDef.units = _.uniqBy(dimensionDef.units, units => units.id);

    dimensionDef.defaultUnit = (dimensionDef.units.find(unit => unit.Scale === 1) || dimensionDef.units[0]).id;
    return dimensionDef;
  });

  const data = {
    dimensions,
  };

  const templateFile = path.join(__dirname, 'units.ejs');
  const compiled = await ejs.renderFile(templateFile, data, {});

  await fs.promises.writeFile(outputFile, compiled);
}

genUnits(path.join(__dirname, '..', '..', 'src', 'units', 'units.ts'));

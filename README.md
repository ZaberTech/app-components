# Zaber App Components

Library of components specific to Zaber that are used across our projects.

## Development

To test out this project use:
```
npm run build; cp -rf ./lib ../zaber-launcher/node_modules/@zaber/app-components
```

## Installation

The package can be installed from the internal gitlab package registry (installed as `@zaber/app-components`)

```
echo @software-public:registry=https://gitlab.izaber.com/api/v4/packages/npm/ >> .npmrc
npm i @zaber/app-components@npm:@software-public/app-components
```

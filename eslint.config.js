const tsEslint = require('typescript-eslint');
const { includeIgnoreFile } = require('@eslint/compat');
const path = require('node:path');
const { config: zaberEslintConfig } = require('@zaber/eslint-config');

module.exports = tsEslint.config(
  includeIgnoreFile(path.resolve(__dirname, '.gitignore')),
  {
    ignores: ['generated/units/'],
  },
  {
    files: ['**/*.js'],
    extends: [zaberEslintConfig.javascript],
  },
  {
    files: ['**/*.ts'],
    extends: [zaberEslintConfig.typescript()],
    rules: {
      '@typescript-eslint/unbound-method': ['error', { ignoreStatic: true }],
      '@typescript-eslint/prefer-nullish-coalescing': ['error', { ignorePrimitives: true }],
      '@typescript-eslint/no-misused-promises': ['error', { checksVoidReturn: false }],
    }
  },
  {
    files: ['**/*.test.ts', '**/*.mock.ts'],
    extends: [zaberEslintConfig.test],
  }
);

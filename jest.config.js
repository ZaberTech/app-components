module.exports = {
  preset: 'ts-jest',
  setupFilesAfterEnv: ['./src/test/setup.ts'],
  moduleDirectories: [
    'node_modules',
  ],
};
